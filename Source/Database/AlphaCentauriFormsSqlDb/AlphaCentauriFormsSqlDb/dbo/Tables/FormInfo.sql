﻿CREATE TABLE [dbo].[FormInfo] (
    [Id]            UNIQUEIDENTIFIER CONSTRAINT [DF_FormInfo_Id] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [SourceUri]     NVARCHAR (MAX)   NULL,
    [CustomerId]    UNIQUEIDENTIFIER NULL,
    [CustomerScope] NVARCHAR (MAX)   NULL,
    [ListId]        UNIQUEIDENTIFIER NULL,
    [ContentTypeId] UNIQUEIDENTIFIER NULL,
    [Contents]      NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_FormInfo] PRIMARY KEY CLUSTERED ([Id] ASC)
);

