﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AzureResearch.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace AzureResearch
{
    [TestClass]
    public class UnitTest1
    {
        private static readonly string databaseName = ConfigurationManager.AppSettings["DatabaseId"];
        private static readonly string endpointUrl = ConfigurationManager.AppSettings["EndPointUrl"];
        private static readonly string authorizationKey = ConfigurationManager.AppSettings["AuthorizationKey"];
        private static readonly ConnectionPolicy connectionPolicy = new ConnectionPolicy { UserAgentSuffix = " samples-net/3" };

        private DocumentClient client;

        [TestMethod]
        public async Task TestMethod1()
        {
            client = new DocumentClient(new Uri(endpointUrl), authorizationKey);

            await this.RunDatabaseDemo();
        }

        /// <summary>
        /// Run basic database metadata operations as a console app.
        /// </summary>
        /// <returns></returns>
        private async Task RunDatabaseDemo()
        {
            await CreateDatabaseIfNotExists();

            Database database = await client.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(databaseName));
            Console.WriteLine("\n3. Read a database resource: {0}", database);

            Console.WriteLine("\n4. Reading all databases resources for an account");
            foreach (var db in await client.ReadDatabaseFeedAsync())
            {
                Console.WriteLine(db);
            }

            await this.CreateDocumentCollectionIfNotExists(databaseName, "users");


            var userInfo = new UserInfo
            {
                Id = Guid.NewGuid().ToString(),
                FirstName = "Li",
                LastName = "Wang",
                Address = new Address
                {
                    AddressLine1 = "35 Moon Ave",
                    AddressLine2 = "N/A",
                    Suburb = "Glen Wav",
                    PostCode = "3150"
                }
            };

            await this.CreateUserIfNotExists(databaseName, "users", userInfo);

            this.QueryUsers(databaseName, "users");

            ////await client.DeleteDatabaseAsync(UriFactory.CreateDatabaseUri(databaseName));
            ////Console.WriteLine("\n5. Database {0} deleted.", database.Id);
        }

        /// <summary>
        /// Create a database if it doesn't exist.
        /// </summary>
        /// <returns></returns>
        private async Task CreateDatabaseIfNotExists()
        {
            Database database = client.CreateDatabaseQuery().Where(db => db.Id == databaseName).AsEnumerable().FirstOrDefault();
            Console.WriteLine("1. Query for a database returned: {0}", database == null ? "no results" : database.Id);

            //check if a database was returned
            if (database == null)
            {
                database = await client.CreateDatabaseAsync(new Database { Id = databaseName });
                Console.WriteLine("\n2. Created Database: id - {0}", database.Id);
            }
        }

        private async Task CreateUserIfNotExists(string databaseName, string collectionName, UserInfo user)
        {
            DocumentClientException de = null;
            try
            {
                await this.client.ReadDocumentAsync(UriFactory.CreateDocumentUri(databaseName, collectionName, user.Id));
                this.WriteToConsoleAndPromptToContinue("Found {0}", user.Id);
            }
            catch (DocumentClientException ex)
            {
                de = ex;
            }

            if (de != null)
            {
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    await this.client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), user);
                    this.WriteToConsoleAndPromptToContinue("Created UserInfo {0}", user.Id);
                }
                else
                {
                    throw de;
                }
            }
        }

        private void QueryUsers(string databaseName, string collectionName)
        {
            var userInfos =
                client.CreateDocumentQuery<UserInfo>(
                    UriFactory.CreateDocumentCollectionUri(databaseName, collectionName), "SELECT * FROM Users usr WHERE usr.Address.PostCode = '3150'",
                    new FeedOptions {EnableScanInQuery = true});

            var list = userInfos.ToList();

            Console.WriteLine("Result:" + JsonConvert.SerializeObject(list));
        }

        private async Task CreateDocumentCollectionIfNotExists(string databaseName, string collectionName)
        {
            DocumentClientException de = null;
            try
            {
                await this.client.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(databaseName, collectionName));
                this.WriteToConsoleAndPromptToContinue("Found {0}", collectionName);
            }
            catch (DocumentClientException ex)
            {
                de = ex;
            }

            if (de != null)
            {

                // If the document collection does not exist, create a new collection
                if (de.StatusCode == HttpStatusCode.NotFound)
                {
                    DocumentCollection collectionInfo = new DocumentCollection
                    {
                        Id = collectionName,
                        IndexingPolicy = new IndexingPolicy(new RangeIndex(DataType.String) { Precision = -1 })
                    };

                    // Configure collections for maximum query flexibility including string range queries.

                    // Here we create a collection with 400 RU/s.
                    await this.client.CreateDocumentCollectionAsync(
                        UriFactory.CreateDatabaseUri(databaseName),
                        new DocumentCollection { Id = collectionName },
                        new RequestOptions { OfferThroughput = 400 });

                    this.WriteToConsoleAndPromptToContinue("Created {0}", collectionName);
                }
                else
                {
                    throw de;
                }
            }
        }

        private void WriteToConsoleAndPromptToContinue(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }
    }
}
