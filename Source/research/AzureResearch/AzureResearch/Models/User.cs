﻿using System;

namespace AzureResearch.Models
{
    public class UserInfo
    {
        public string Id { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Address Address { get; set; }

        
    }

    public class Address
    {
        
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Suburb { get; set; }

        public string PostCode { get; set; } 
        
    }
}

