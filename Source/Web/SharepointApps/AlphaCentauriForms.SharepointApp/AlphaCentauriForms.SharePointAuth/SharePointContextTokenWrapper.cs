﻿using Microsoft.IdentityModel.S2S.Tokens;

namespace AlphaCentauriForms.SharePointAuth
{
    public class SharePointContextTokenWrapper
    {
        public SharePointContextToken SharePointContextToken { get; set; }

        public JsonWebSecurityTokenHandler JsonWebSecurityTokenHandler { get; set; }

        public JsonWebSecurityToken JsonWebSecurityToken { get; set; }
    }
}