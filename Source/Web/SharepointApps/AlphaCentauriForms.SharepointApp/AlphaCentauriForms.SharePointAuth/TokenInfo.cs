﻿using System;

namespace AlphaCentauriForms.SharePointAuth
{
    public class TokenInfo
    {
        public TokenInfo(string token, DateTime expiration)
        {
            this.Token = token;
            this.Expiration = expiration;
        }

        public string Token { get; set; }

        public DateTime Expiration { get; set; }
    }
}