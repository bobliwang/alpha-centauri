﻿<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" language="C#" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<WebPartPages:AllowFraming ID="AllowFraming1" runat="server" />

<html>
    <head>
        
        <title>Alpha Centari Forms</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        
        <script src="~remoteAppUrl/app/sp/launchFromSharePoint.js" type="text/javascript"></script>
    </head>
    
    <body style="margin: 0px; padding: 0px;">
        <SharePoint:SPAppIFrame ID="SPAppIFrame1" runat="server" frameborder="0"></SharePoint:SPAppIFrame>
    </body>
</html>