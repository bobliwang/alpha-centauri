﻿using System.Net;
using RestSharp;

namespace SharePointO365.RestClient.Auth
{
    public class SpoidcrlAuthProvider : IAuthProvider
    {
        public SpoidcrlAuthProvider(string authority, string spoidcrl)
        {
            this.Authority = authority;
            this.Spoidcrl = spoidcrl;
        }

        public string Spoidcrl { get; set; }

        public string Authority { get; set; }

        public void SetupAuth(IRestClient client)
        {
            var cookieContainer = new CookieContainer();
            cookieContainer.Add(new Cookie("SPOIDCRL", this.Spoidcrl, string.Empty, this.Authority));
            client.CookieContainer = cookieContainer;
        }
    }
}
