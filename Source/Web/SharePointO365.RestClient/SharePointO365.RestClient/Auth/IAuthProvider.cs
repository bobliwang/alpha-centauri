using RestSharp;

namespace SharePointO365.RestClient.Auth
{
    public interface IAuthProvider
    {
        void SetupAuth(IRestClient client);
    }
}