using RestSharp;

namespace SharePointO365.RestClient.Auth
{
    public class AccessTokenAuthProvider : IAuthProvider
    {
        public AccessTokenAuthProvider(string accessToken)
        {
            this.AccessToken = accessToken;
        }

        public string AccessToken { get; set; }

        public void SetupAuth(IRestClient client)
        {
            client.AddDefaultHeader("Authorization", "Bearer " + this.AccessToken);
        }
    }
}