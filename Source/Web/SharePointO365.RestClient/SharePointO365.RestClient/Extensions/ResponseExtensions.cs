﻿using RestSharp;
using SharePointO365.RestClient.Exceptions;

namespace SharePointO365.RestClient.Extensions
{
    public static class ResponseExtensions
    {
        public static void EnsureSuccess(this IRestResponse response)
        {
            var statusInVal = ((int) response.StatusCode);

            if (statusInVal < 200 || statusInVal > 299)
            {
                if (response.ErrorException != null)
                {
                    throw new FailedStatusCodeException(string.Format("Invalid Status {0} - Content: {1}", response.StatusCode, response.Content), response.ErrorException, response.StatusCode);
                }

                throw new FailedStatusCodeException(string.Format("Invalid Status {0} - Content: {1}", response.StatusCode, response.Content), response.StatusCode);
            }
        }
    }
}