﻿namespace SharePointO365.RestClient.Extensions
{
    public static class StringExtensions
    {
        public static string AsSafeOdata(this string odataPart)
        {
            return odataPart.Replace("'", "''");
        }
    }
}