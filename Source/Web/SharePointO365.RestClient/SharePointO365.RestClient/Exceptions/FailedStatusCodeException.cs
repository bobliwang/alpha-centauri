﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace SharePointO365.RestClient.Exceptions
{
    public class FailedStatusCodeException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        public FailedStatusCodeException(HttpStatusCode statusCode)
        {
            this.StatusCode = statusCode;
        }

        public FailedStatusCodeException(string message, HttpStatusCode statusCode) : base(message)
        {
            this.StatusCode = statusCode;
        }

        public FailedStatusCodeException(string message, Exception innerException, HttpStatusCode statusCode) : base(message, innerException)
        {
            this.StatusCode = statusCode;
        }

        protected FailedStatusCodeException(SerializationInfo info, StreamingContext context, HttpStatusCode statusCode) : base(info, context)
        {
            this.StatusCode = statusCode;
        }
    }
}