using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using RestSharp;
using SharePointO365.RestClient.Auth;
using SharePointO365.RestClient.Exceptions;
using SharePointO365.RestClient.Extensions;
using SharePointO365.RestClient.Models;
using SharePointO365.RestClient.Serialization;
using Tangsem.Common.Extensions;

namespace SharePointO365.RestClient
{
    public class SpoRestClient
    {
        public SpoRestClient(string spHostUrl, IAuthProvider authProvider)
        {
            this.SpHostUrl = spHostUrl;
            this.RestClient = new RestSharp.RestClient(this.SpHostUrl + "/_api/");
            this.RestClient.AddHandler("application/json", NewtonsoftJsonSerializer.Default);
            this.RestClient.AddHandler("text/json", NewtonsoftJsonSerializer.Default);
            this.RestClient.AddHandler("text/x-json", NewtonsoftJsonSerializer.Default);
            this.RestClient.AddHandler("text/javascript", NewtonsoftJsonSerializer.Default);
            this.RestClient.AddHandler("*+json", NewtonsoftJsonSerializer.Default);


            this.AuthProvider = authProvider;
            this.AuthProvider.SetupAuth(this.RestClient);
        }

        public IAuthProvider AuthProvider { get; set; }

        public string SpHostUrl { get; set; }

        public object Credentials { get; set; }

        ////public readonly ApiEndpoints ApiEndpoints = new ApiEndpoints();

        public RestSharp.RestClient RestClient { get; set; }

        ////public async Task<T> GetInfo<T>(IApiEndPoint apiEndPoint, bool ensureSuccess = true)
        ////{
        ////    var response = await this.RestClient.ExecuteTaskAsync<T>(apiEndPoint.CreateRequest());

        ////    if (ensureSuccess)
        ////    {
        ////        response.EnsureSuccess();
        ////    }

        ////    return response.Data;
        ////}

        public async Task<T> GetInfo<T>(string url, Method method = Method.GET, bool ensureSuccess = true)
        {
            var response = await this.RestClient.ExecuteTaskAsync<T>(new RestRequest(url, method));

            if (ensureSuccess)
            {
                response.EnsureSuccess();
            }

            return response.Data;
        }

        public async Task<WebInfo> GetWebInfo()
        {
            return await this.GetInfo<WebInfo>("web");
        }

        public async Task<UserInfo> GetCurrentUserInfo()
        {
            return await this.GetInfo<UserInfo>("web/currentUser");
        }

        public async Task<ListInfo> GetListInfo(string listTitle)
        {
            return await this.GetInfo<ListInfo>("web/lists/getbytitle('{0}')".FormatBy(listTitle));
        }

        public async Task<ListInfo> GetListInfo(Guid listId)
        {
            return await this.GetInfo<ListInfo>("web/lists/getbyid('{0}')".FormatBy(listId));
        }

        public async Task<FieldsInfo> GetFieldsInfo(string listTitle)
        {
            return await this.GetInfo<FieldsInfo>("web/lists/getbytitle('{0}')/fields".FormatBy(listTitle));
        }

        public async Task<FieldsInfo> GetFieldsInfo(Guid listId)
        {
            return await this.GetInfo<FieldsInfo>("web/lists/getbyid('{0}')/fields".FormatBy(listId));
        }

        public async Task<ContentTypeInfos> GetListContentTypes(string listTitle)
        {
            return await this.GetInfo<ContentTypeInfos>("web/lists/getbytitle('{0}')/contenttypes".FormatBy(listTitle));
        }

        public async Task<ContentTypeInfos> GetListContentTypes(Guid listId)
        {
            return await this.GetInfo<ContentTypeInfos>("web/lists/getbyid('{0}')/contenttypes".FormatBy(listId));
        }

        public async Task<FieldInfos> GetListContentTypeFields(Guid listId, string listContentTypeId)
        {
            return await this.GetInfo<FieldInfos>("web/lists/getbyid('{0}')/contenttypes/getbyid('{1}')/fields".FormatBy(listId, listContentTypeId));
        }

        public async Task<ContentTypeInfos> GetListContentTypeFields(string listTitle, string listContentTypeId)
        {
            return await this.GetInfo<ContentTypeInfos>("web/lists/getbytitle('{0}')/contenttypes/getbyid('{1}')/fields".FormatBy(listTitle, listContentTypeId));
        }

        public async Task<AppInstanceInfo> GetAppInstanceByProductIdAsync(Guid productId)
        {
            var resourceUri = @"web/getAppInstancesByProductId('{0}')".FormatBy(productId);
            var result = await this.GetAppDetails(resourceUri);

            return result;
        }
        
        public async Task AddListItem(Guid listId, ListItemData listItemData)
        {
            var byExpr = "getbyid('{0}')".FormatBy(listId);

            await this.AddListItemByExpr(byExpr, listItemData);
        }

        public async Task AddListItem(string listTitle, ListItemData listItemData)
        {
            var byExpr = "getbytitle('{0}')".FormatBy(listTitle);

            await this.AddListItemByExpr(byExpr, listItemData);
        }

        public async Task UpdateListContentTypeUrls(Guid listId, string contentTypeId, ContentTypeUrlsInfo newUrls)
        {
            var requestUri = string.Format("Web/Lists(guid'{0}')/ContentTypes('{1}')", listId, contentTypeId);

            var request = new RestRequest(requestUri, Method.POST);
            request.AddJsonBody(newUrls);
            request.AddHeader("X-HTTP-Method", "PATCH");

            var response = await this.RestClient.ExecuteTaskAsync(request);

            response.EnsureSuccess();
        }

        public async Task<ListItems> GetListData(Guid listId)
        {
            var byExpr = "getbyid('{0}')".FormatBy(listId);

            return await this.GetInfo<ListItems>("web/lists/{0}/items".FormatBy(byExpr));
        }

        private async Task AddListItemByExpr(string byExpr, ListItemData listItemData)
        {
            var formDigest = await this.GetFormDigest();

            var request = new RestRequest("web/lists/{0}/items".FormatBy(byExpr), Method.POST);
            request.AddJsonBody(listItemData);
            request.AddHeader("X-RequestDigest", formDigest);

            var response = await this.RestClient.ExecuteTaskAsync(request);

            response.EnsureSuccess();
        }

        public async Task<Guid> GetTenantId()
        {
            var newClient = new RestSharp.RestClient(this.SpHostUrl);
            var request = new RestRequest(@"_vti_bin/client.svc", Method.GET);
            request.AddHeader("Authorization", "Bearer");

            var response = await newClient.ExecuteTaskAsync(request);

            var header = response.Headers.FirstOrDefault(x => "WWW-Authenticate".Equals(x.Name, StringComparison.InvariantCultureIgnoreCase)).Value.ToString();
            var key = @"Bearer realm=""";
            var realm = header.Substring(header.IndexOf(key) + key.Length, 36);

            return new Guid(realm);
        }

        private async Task<AppInstanceInfo> GetAppDetails(string resourceUri)
        {

            var appInstanceInfos = await this.GetInfo<AppInstanceInfos>(resourceUri);


            var formApp = appInstanceInfos.Value.First(instance => instance.Status == (int)AppInstanceStatus.Installed);
            formApp.RemoteAppUrl = new Uri(formApp.StartPage).GetLeftPart(UriPartial.Authority);
            formApp.AppWebUrl = formApp.AppWebFullUrl;
            var appWebArray = formApp.AppWebUrl.Replace("//", string.Empty).Split('/');

            formApp.AppName = appWebArray[appWebArray.Length - 1];
            formApp.HostUrl = this.SpHostUrl;

            return formApp;
        }

        private async Task<string> GetFormDigest()
        {
            var digestResponse = await this.RestClient.ExecuteTaskAsync<string>(new RestRequest("contextinfo", Method.POST));

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(digestResponse.Content);

            var digestElement = xmlDoc.DocumentElement.GetElementsByTagName("d:FormDigestValue").Cast<XmlElement>().FirstOrDefault();
            
            var formDigest = digestElement.InnerText;
            return formDigest;
        }

        class GetBy
        {
            static string Title(string title)
            {
                return "getbytitle('{0}')".FormatBy(title);
            }

            static string Id(string id)
            {
                return "getbyid('{0}')".FormatBy(id);
            }
        }

    }
}