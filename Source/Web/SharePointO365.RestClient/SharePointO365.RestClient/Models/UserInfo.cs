﻿namespace SharePointO365.RestClient.Models
{
    public class UserInfo
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Email { get; set; }

        public string IsSiteAdmin { get; set; }

        public string LoginName { get; set; }
    }
}