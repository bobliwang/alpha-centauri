﻿namespace SharePointO365.RestClient.Models
{
    public class ODataSetting
    {
        public string EditLink { get; set; }

        public string Id{ get; set; }

        public string Type { get; set; }
    }
}