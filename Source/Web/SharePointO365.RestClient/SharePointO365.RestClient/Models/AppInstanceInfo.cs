﻿namespace SharePointO365.RestClient.Models
{
    public class AppInstanceInfo
    {
        public string AppWebFullUrl { get; set; }

        public string RemoteAppUrl { get; set; }

        public string StartPage { get; set; }

        public int Status { get; set; }

        public string AppName { get; set; }

        public string AppWebUrl { get; set; }

        public string HostUrl { get; set; }
    }

    public enum AppInstanceStatus
    {
        InvalidStatus = 0,
        Installing = 1,
        Uninstalling = 4,
        Installed = 5,
        Canceling = 7,
        Upgrading = 8,
        Initialized = 9,
        UpgradeCanceling = 10,
        Disabling = 11,
        Disabled = 12,
        SecretRolling = 13,
        Recycling = 14,
        Recycled = 15,
        Restoring = 16,
        RestoreCanceling = 17,
    }
}