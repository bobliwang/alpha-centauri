﻿namespace SharePointO365.RestClient.Models
{
    public class ContentTypeUrlsInfo
    {
        public string NewFormUrl { get; set; }

        public string EditFormUrl { get; set; }

        public string DisplayFormUrl { get; set; }

        public string MobileDisplayFormUrl { get; set; }

        public string MobileEditFormUrl { get; set; }

        public string MobileNewFormUrl { get; set; }

        public void ResetUrls()
        {
            this.NewFormUrl = string.Empty;
            this.EditFormUrl = string.Empty;
            this.DisplayFormUrl = string.Empty;
            this.MobileDisplayFormUrl = string.Empty;
            this.MobileEditFormUrl = string.Empty;
            this.MobileNewFormUrl = string.Empty;
        }
    }
}