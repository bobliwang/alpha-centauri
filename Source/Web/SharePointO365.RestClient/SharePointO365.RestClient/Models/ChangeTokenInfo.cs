using System;

namespace SharePointO365.RestClient.Models
{
    public class ChangeTokenInfo
    {
        public string StringValue { get; set; }

        public int? VersionNumber
        {
            get
            {
                if (this.StringValue == null)
                {
                    return null;
                }

                return Convert.ToInt32(this.StringValue.Split(';')[0]);
            }
        }

        public int? Scope
        {
            get
            {
                if (this.StringValue == null)
                {
                    return null;
                }

                return Convert.ToInt32(this.StringValue.Split(';')[1]);
            }
        }

        public Guid? ScopeId
        {
            get
            {
                if (this.StringValue == null)
                {
                    return null;
                }

                return new Guid(this.StringValue.Split(';')[2]);
            }
        }

        public DateTime? ChangeDate
        {
            get
            {
                if (this.StringValue == null)
                {
                    return null;
                }

                return new DateTime(Convert.ToInt64(this.StringValue[3]));
            }
        }

        public long? ChangeNumber
        {
            get
            {
                if (this.StringValue == null)
                {
                    return null;
                }

                return Convert.ToInt64(this.StringValue[4]);
            }
        }

        
    }
}