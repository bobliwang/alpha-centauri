﻿namespace SharePointO365.RestClient.Models
{
    public class ListItem
    {
        public int Id { get; set; } 

        public string Title { get; set; }
    }
}