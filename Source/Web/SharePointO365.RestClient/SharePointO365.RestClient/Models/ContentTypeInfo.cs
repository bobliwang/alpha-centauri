﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json;

namespace SharePointO365.RestClient.Models
{
    public class InfoArray<T>
    {
        public IList<T> Value { get; set; }
    }

    public class ListItems: InfoArray<ListItem>
    {
    }

    public class ContentTypeInfos : InfoArray<ContentTypeInfo>
    {
    }

    public class UserInfos : InfoArray<UserInfo>
    {
    }

    public class FieldInfos : InfoArray<FieldInfo>
    {
    }

    public class AppInstanceInfos : InfoArray<AppInstanceInfo>
    {
    }

    public class ContentTypeInfo
    {
        private string _contentTypeFolder;

        public string Description { get; set; }

        public string DisplayFormTemplateName { get; set; }
        public string DisplayFormUrl { get; set; }
        public string DocumentTemplate { get; set; }
        public string DocumentTemplateUrl { get; set; }
        public string EditFormTemplateName { get; set; }
        public string EditFormUrl { get; set; }
        public string Group { get; set; }
        public bool Hidden { get; set; }

        public Id Id { get; set; }

        public string  JSLink { get; set; }

        public string MobileDisplayFormUrl{ get; set; }

        public string MobileFormUrl { get; set; }

        public string MobileNewFormUrl { get; set; }

        public string Name { get; set; }

        public string NewFormTemplateName { get; set; }

        public string NewFormUrl { get; set; }

        public string ReadOnly { get; set; }

        public string SchemaXml { get; set; }

        public string Scope { get; set; }

        public bool Seald { get; set; }

        public string StringId { get; set; }

        public IList<FieldInfo> Fields { get; set; }

        [JsonIgnore]
        public string ContentTypeFolder
        {
            get
            {
                if (string.IsNullOrEmpty(this._contentTypeFolder))
                {
                    this._contentTypeFolder = this.GetContentTypeFolder();
                }

                return this._contentTypeFolder;
            }
        }

        [JsonIgnore]
        public string FormPath
        {
            get
            {
                return this.Scope + "/" + this.ContentTypeFolder;
            }
        }

        private string GetContentTypeFolder()
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(this.SchemaXml);

            var contentTypeFolder = xmlDoc.GetElementsByTagName("Folder")[0];

            if (xmlDoc.FirstChild != null && contentTypeFolder != null)
            {
                return contentTypeFolder.Attributes["TargetName"].Value;
            }

            return string.Empty;
        }
    }

    public class Id
    {
        public string StringValue { get; set; }
    }

}