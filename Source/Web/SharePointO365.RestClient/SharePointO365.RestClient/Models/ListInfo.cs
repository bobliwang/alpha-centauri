﻿using System;
using System.Collections.Generic;

namespace SharePointO365.RestClient.Models
{
    public class ListInfo
    {
        public string Title { get; set; }

        public int ItemCount { get; set; }

        public bool IsPrivate { get; set; }

        public Guid Id { get; set; }

        public string ListItemEntityTypeFullName { get; set; }

        public ChangeTokenInfo CurrentChangeToken { get; set; }

        public IList<ContentTypeInfo> ContentTypes { get; set; }
    }
}