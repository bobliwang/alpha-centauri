﻿using System.Collections.Generic;

namespace SharePointO365.RestClient.Models
{
    public class FieldsInfo
    {
        public IList<FieldInfo> Value { get; set; }
    }
}