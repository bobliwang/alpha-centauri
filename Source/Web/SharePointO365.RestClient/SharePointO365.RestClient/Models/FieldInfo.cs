﻿using System;
using SharePointO365.RestClient.Enums;

namespace SharePointO365.RestClient.Models
{
    public class FieldInfo
    {
        public Guid Id { get; set; }

        public bool AutoIndex { get; set; }

        public bool CanBeDeleted { get; set; }

        public string DefaultValue { get; set; }

        public string Direction { get; set; }

        public bool EnforceUniqueValues { get; set; }

        public string EntityPropertyName { get; set; }

        public bool Filterable { get; set; }

        public bool FromBaseType { get; set; }

        public string Group { get; set; }

        public string Title { get; set; }

        public bool Indexed { get; set; }

        public string JSLink { get; set; }

        public bool Required { get; set; }
        
        public string TypeAsString { get; set; }

        public string StaticName { get; set; }

        public string InternalName { get; set; }

        public int FieldTypeKind { get; set; }

        public bool Hidden { get; set; }

        public string SchemaXml { get; set; }

        public string SchemaXmlWithResourceTokens { get; set; }

        public string Scope { get; set; }

        public bool Sealed { get; set; }

        public bool Sortable { get; set; }

        public string TypeDisplayName { get; set; }

        public string TypeShortDescription { get; set; }

        public bool ReadOnlyField { get; set; }

        public string ValidationFormula { get; set; }

        public string ValidationMessage { get; set; }

        public FieldType FieldType
        {
            get { return (FieldType) this.FieldTypeKind; }
        }
    }
}