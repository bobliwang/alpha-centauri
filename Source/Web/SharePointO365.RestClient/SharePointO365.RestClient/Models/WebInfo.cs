﻿namespace SharePointO365.RestClient.Models
{
    public class WebInfo
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public string ServerRelativeUrl { get; set; }
    }

   
}