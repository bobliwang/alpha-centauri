﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;

namespace SharePointO365.RestClient.Extensions
{
    public class SpoidcrlGeneartor
    {
        public string Generate(string sharepointHostUrl, string username, string password, bool removePrefix = true)
        {
            var credientials = new SharePointOnlineCredentials(username, this.CreateSecureString(password));
            var str = credientials.GetAuthenticationCookie(new Uri(sharepointHostUrl));

            if (removePrefix)
            {
                str = str.Substring("SPOIDCRL=".Length);
            }

            return str;
        }

        private SecureString CreateSecureString(string text)
        {
            var secureStr = new SecureString();
            if (text.Length > 0)
            {
                foreach (var c in text)
                {
                    secureStr.AppendChar(c);
                }
            }

            return secureStr;
        }
    }
}
