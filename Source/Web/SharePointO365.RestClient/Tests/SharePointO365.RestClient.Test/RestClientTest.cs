﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharePointO365.RestClient.Auth;
using SharePointO365.RestClient.Extensions;
using SharePointO365.RestClient.Models;
using Xunit;

namespace SharePointO365.RestClient.Test
{   
    public class RestClientTest
    {
        private ListInfo ProductList = new ListInfo
        {
            Id = new Guid("721A3EE0-E4EE-433C-9BCF-D8BB37638B88"),
            Title = "ProductList"
        };


        [Fact(DisplayName = "Get Web Info Test")]
        public async void GetWebInfoTest()
        {
            var client = this.GetSharepointRest();
            var webInfo = await client.GetWebInfo();

            Assert.Equal("/sites/dsmig", webInfo.ServerRelativeUrl);
        }

        [Fact(DisplayName = "Get Current User Info Test")]
        public async void GetUserInfoTest()
        {
            var client = this.GetSharepointRest();
            var userInfo = await client.GetCurrentUserInfo();

            Assert.Equal("i:0#.f|membership|liwang@liwangdev.onmicrosoft.com", userInfo.LoginName);
            Assert.Equal("Li Wang", userInfo.Title);
        }

        [Fact(DisplayName = "Get ListInfo by Title Test")]
        public async void GetListInfoByTitleTest()
        {
            var client = this.GetSharepointRest();
            var listInfo = await client.GetListInfo(ProductList.Title);

            Assert.Equal(ProductList.Title, listInfo.Title);
        }

        [Fact(DisplayName = "Get ListInfo by Id Test")]
        public async void GetListInfoByIdTest()
        {
            var client = this.GetSharepointRest();
            var listInfo = await client.GetListInfo(ProductList.Id);

            Assert.Equal(ProductList.Title, listInfo.Title);
        }

        [Fact(DisplayName = "Get Fields Info by List Title Test")]
        public async void GetFieldsInfoByListTitle()
        {
            var client = this.GetSharepointRest();
            var fieldsInfo = await client.GetFieldsInfo(ProductList.Title);

            var fields = fieldsInfo.Value.Where(x => !x.ReadOnlyField && !x.Hidden).ToList();

            Assert.NotEmpty(fieldsInfo.Value);
        }

        [Fact(DisplayName = "Get ListContentTypes by List Title Test")]
        public async void GetListContentTypesByListTitle()
        {
            var client = this.GetSharepointRest();
            var contentTypeInfos = await client.GetListContentTypes(ProductList.Title);
            Assert.NotEmpty(contentTypeInfos.Value);
        }

        [Fact(DisplayName = "Get ListContentTypes by List Id Test")]
        public async void GetListContentTypesByListId()
        {
            var client = this.GetSharepointRest();
            var contentTypeInfos = await client.GetListContentTypes(this.ProductList.Id);
            Assert.NotEmpty(contentTypeInfos.Value);
        }


        [Fact(DisplayName = "Get Fields Info by List Id Test")]
        public async void GetFieldsInfoByListId()
        {
            var client = this.GetSharepointRest();
            var fieldsInfo = await client.GetFieldsInfo(this.ProductList.Id);

            Assert.NotEmpty(fieldsInfo.Value);
        }

        [Fact(DisplayName = "Test Adding List Item")]
        public async void TestAddListItem()
        {
            var client = this.GetSharepointRest();

            var listInfo = await client.GetListInfo(this.ProductList.Id);

            var item = new ListItemData(listInfo.ListItemEntityTypeFullName);

            item["Title"] = "Chocolate";
            item["ProductCode"] = Guid.NewGuid().ToString();

            await client.AddListItem(this.ProductList.Title, item);
        }


        private SpoRestClient GetSharepointRest()
        {
            var uri = new Uri("https://liwangdev.sharepoint.com/sites/dsmig");

            ////var spoidcrl = new SpoidcrlGeneartor().Generate(uri.ToString(), "username", "password");

            var spoidcrl = "77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U1A+VHJ1ZSwwaC5mfG1lbWJlcnNoaXB8MTAwMzdmZmU5NjVkOTZlMUBsaXZlLmNvbSwwIy5mfG1lbWJlcnNoaXB8bGl3YW5nQGxpd2FuZ2Rldi5vbm1pY3Jvc29mdC5jb20sMTMxMDI0MDgwMjE4NjcwOTc0OzEzMTAwOTE3NzU5MDAwMDAwMCxGYWxzZSxMN1JCKzFzT0pDTTh6MnIxTXdzYSt2RkxobkNWakNweHFwVjZ2cDVIMCs1NFJNRm1jbzdzWG9VWThXcjlMVnpCd25mWGkwWmtiWXJoRFdoYTAybTk4S1BtZUFLUUUwbGNlVjd3WUdibEJ6bDkvMkt0UVFzSXNoZjg1OFlzZ21mcW9NM2hBNGVmMnZ0a1BRVmQvWEppVm1MOS8remdoMTF6b2FreGcxbVJ0eFZleTh3NTNLam5RbVQzOFA4WlBTUTFVK016cXl3L0NQdmdmaGg4Zyt3Z293cG1LWC9BZ00yMTJ6MnJ0bk11OURCM25qOFhwQkNBdEJqNmF6MjA0NGFFTXRxS0p1U1lNNnR2R1diNjBYOHF1SHFya3Fvc0diR1VtY0Q5Rk1VdEN0dlN3azEvS2NOb0lReWErYkFtSXdFRHFNSGpHMU9FYUdKMitPWlNFbXdPVEE9PSxodHRwczovL2xpd2FuZ2Rldi5zaGFyZXBvaW50LmNvbS9fdnRpX2Jpbi9pZGNybC5zdmMvPC9TUD4=";

            var authProvider = new SpoidcrlAuthProvider(uri.Authority, spoidcrl);

            var client = new SpoRestClient(uri.ToString(), authProvider);
            return client;
        }
    }
}
