using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;

using Tangsem.Data.Domain;
using Tangsem.NHibernate.Domain;
using FormsBuilder.Common.Domain.Entities;

namespace FormsBuilder.Common.Domain.Repositories.NHibernate
{ 
  /// <summary>
  /// The FormRepository class.
  /// </summary>
  public partial class FormRepository : RepositoryBase, IFormRepository
  {

    
    /// <summary>
    /// The IQueryable for FormInfos.
    /// </summary>
    public virtual IQueryable<FormInfo> FormInfos
    {
      get
      {
        return this.GetEntities<FormInfo>();
      }
    }

        
    

    
    /// <summary>
    /// Get FormInfo by primary key.
    /// </summary>
    public virtual FormInfo LookupFormInfoById(System.Guid id)
    {
      return this.LookupById<FormInfo>(id);
    }
    
    /// <summary>
    /// Delete FormInfo by primary key.
    /// </summary>
    public virtual FormInfo DeleteFormInfoById(System.Guid id)
    {
      return this.DeleteById<FormInfo>(id);
    }
    
    /// <summary>
    /// Save a new FormInfo instance.
    /// </summary>
    public virtual FormInfo SaveFormInfo(FormInfo formInfo)
    {
      return this.Save<FormInfo>(formInfo);
    }
    
    /// <summary>
    /// Update an existing FormInfo instance.
    /// </summary>
    public virtual FormInfo UpdateFormInfo(FormInfo formInfo)
    {
      return this.Update<FormInfo>(formInfo);
    }
    
    /// <summary>
    /// Save or update an existing FormInfo instance.
    /// </summary>
    public virtual FormInfo SaveOrUpdateFormInfo(FormInfo formInfo)
    {
      return this.SaveOrUpdate<FormInfo>(formInfo);
    }

    
  }
}

/*    
  @NOTE: The following entities are not IAuditableEntities:
     

     
  ------------------------------
  -- FormInfo     ----
  ------------------------------
  ALTER TABLE FormInfo ADD CreatedById INT NULL
  GO
  ALTER TABLE FormInfo ADD ModifiedById INT NULL
  GO
  ALTER TABLE FormInfo ADD CreatedTime DATETIME NULL
  GO
  ALTER TABLE FormInfo ADD ModifiedTime DATETIME NULL
  GO
  ALTER TABLE FormInfo ADD Active BIT NULL
  GO
         
       
  */