using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;

using Tangsem.Data.Domain;

using FormsBuilder.Common.Domain.Entities;

namespace FormsBuilder.Common.Domain.Repositories
{
	/// <summary>
	/// The IFormRepository interface.
	/// </summary>
	public partial interface IFormRepository : IRepository
	{

		
		/// <summary>
		/// Maps to database table/view FormInfo. The IQueryable for FormInfos.
		/// </summary>
		IQueryable<FormInfo> FormInfos { get; }

				
		

		
		/// <summary>
		/// Get FormInfo by primary key.
		/// </summary>
		FormInfo LookupFormInfoById(System.Guid id);
		
		/// <summary>
		/// Delete FormInfo by primary key.
		/// </summary>
		FormInfo DeleteFormInfoById(System.Guid id);
		
		/// <summary>
		/// Save a new FormInfo instance.
		/// </summary>
		FormInfo SaveFormInfo(FormInfo formInfo);
		
		/// <summary>
		/// Update an existing FormInfo instance.
		/// </summary>
		FormInfo UpdateFormInfo(FormInfo formInfo);
		
		/// <summary>
		/// Save or update an existing FormInfo instance.
		/// </summary>
		FormInfo SaveOrUpdateFormInfo(FormInfo formInfo);

		
	}
}