﻿using System;
using System.Collections.Generic;
using FormsBuilder.Common.Controls;
using FormsBuilder.Common.Domain.Metadata;

namespace FormsBuilder.Common.Domain.Form
{
    public class Form
    {
        public Form()
        {
            this.Rows = new List<RowControl>();
        }

        public Guid? Id { get; set; }

        public IList<RowControl> Rows { get; set; }

        public Guid ListId { get; set; }

        public string ContentTypeId { get; set; }

        public IList<Control> GetAllControls()
        {
            var ctrls = new List<Control>();

            foreach (var row in this.Rows)
            {
                ctrls.AddRange(row.Columns);

                foreach (var ctrl in row.Columns)
                {   
                    ctrls.AddRange(this.GetAllSubControls(ctrl));
                }
            }

            return ctrls;
        }


        public IList<Control> GetAllSubControls(Control control)
        {
            var lst = new List<Control>();

            foreach (var subControl in control.Columns)
            {
                lst.Add(subControl);

                lst.AddRange(this.GetAllSubControls(subControl));
            }

            return lst;
        }
    }

    public class RowControl
    {
        public RowControl()
        {
            this.Columns = new List<Controls.Control>();
        }

        public IList<Control> Columns { get; set; }


    }
}