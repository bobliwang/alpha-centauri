﻿namespace FormsBuilder.Common.Domain.Metadata
{
    public class BindingExpression
    {
        public BindingExpression()
        {
            this.BindingExpressionMode = BindingExpressionMode.Client;
        }

        public BindingExpressionMode BindingExpressionMode { get; set; }

        public string Expression { get; set; }
    }
}