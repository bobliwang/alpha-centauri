﻿namespace FormsBuilder.Common.Domain.Metadata
{
    public enum LabelControlMode
    {
        Inline,
        SeparateLine
    }
}