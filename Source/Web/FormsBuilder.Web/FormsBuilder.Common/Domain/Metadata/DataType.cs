﻿namespace FormsBuilder.Common.Domain.Metadata
{
    public enum DataType
    {
        Binary = 1,
        Boolean = 2,
        Byte = 3,
        DateTime = 4,
        DateTimeOffset = 5,
        Decimal = 6,
        Double = 7,
        Float = 8,
        Guid = 9,
        Integer = 10,
        String = 11,
        Image = 12,
        People = 13,
        Lookup = 14,

        Attachments = 15,

        Label = 20,

        CanvasColumnContainer = 1000,
        CanvasRow = 1001,
        Repeater = 1002,

        Rules = 10000
    }
}