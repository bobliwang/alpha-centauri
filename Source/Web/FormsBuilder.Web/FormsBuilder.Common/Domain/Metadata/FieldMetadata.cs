﻿using FormsBuilder.Common.Controls;
using Newtonsoft.Json;

namespace FormsBuilder.Common.Domain.Metadata
{
    public class FieldMetadata
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("dataType")]
        public DataType DataType { get; set; }

        [JsonProperty("dataTypeAsString")]
        public string DataTypeAsString
        {
            get { return this.DataType.ToString(); }
        }

        [JsonProperty("layoutColumns")]
        public int LayoutColumns { get; set; }
        
        [JsonProperty("required")]
        public bool Required { get; set; }
        
#if DEBUG
        [JsonProperty("__rawdata")]
        public object RawData
        {
            get;
            set;
        }
#endif

        public virtual Control CreateControl()
        {
            var control = new Control
            {
                LayoutColumns = 4,
                ControlType = new DataTypeManager().MapToControlType(this.DataType),
                DisplayName = this.DisplayName,
                Name = this.Name
            };

            return control;
        } 
    }

    public class LookupFieldMetadata : FieldMetadata
    {
        [JsonProperty("sourceListName")]
        public string SourceListName { get; set; }

        [JsonProperty("sourceListId")]
        public string SourceListId { get; set; }

        public override Control CreateControl()
        {
            var control = new ListLookup
            {
                LayoutColumns = 4,
                ControlType = new DataTypeManager().MapToControlType(this.DataType),
                DisplayName = this.DisplayName,
                Name = this.Name,

                SourceListId = this.SourceListId,
                SourceListName = this.SourceListName,
            };

            return control;
        }
    }
}