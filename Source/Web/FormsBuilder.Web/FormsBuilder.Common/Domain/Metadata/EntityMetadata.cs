﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace FormsBuilder.Common.Domain.Metadata
{
    public class EntityMetadata : ICacheable
    {
        public EntityMetadata()
        {
            this.Fields = new List<FieldMetadata>();
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("fields")]
        public IList<FieldMetadata> Fields { get; set; }
        
        public Guid ListId { get; set; }

        public string CacheKey
        {
            get { return this.Name; }
        }
    }
}