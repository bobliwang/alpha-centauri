﻿using System.Collections.Generic;
using FormsBuilder.Common.Controls;

namespace FormsBuilder.Common.Domain.Metadata
{
    public class DataTypeToControlMappingProvider
    {
        private static readonly IDictionary<DataType, ControlType> _mappings = new Dictionary<DataType, ControlType>
        {
              { DataType.Boolean               , ControlType.CheckBox              }
            , { DataType.DateTime              , ControlType.DateTimePicker        }
            , { DataType.String                , ControlType.TextBox               }
            , { DataType.Guid                  , ControlType.TextBox               }
            , { DataType.Integer               , ControlType.TextBox               }
            , { DataType.Double                , ControlType.TextBox               }
            , { DataType.Label                 , ControlType.Label                 }
            , { DataType.Binary                , ControlType.FileUpload            }
            , { DataType.Attachments           , ControlType.FileUpload            }
            , { DataType.CanvasColumnContainer , ControlType.CanvasColumnContainer }
            , { DataType.Image                 , ControlType.Image                 }
            , { DataType.People                , ControlType.Peoplepicker          }
        };

        public IDictionary<DataType, ControlType> Mappings
        {
            get { return _mappings; }
        }

    }
}