﻿namespace FormsBuilder.Common.Domain.Metadata
{
    public enum BindingExpressionMode
    {
        Client,
        Server,
    }
}