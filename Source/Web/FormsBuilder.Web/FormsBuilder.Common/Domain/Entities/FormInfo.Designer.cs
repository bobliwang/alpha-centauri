using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;

using Tangsem.Common.Entities.Reos;

namespace FormsBuilder.Common.Domain.Entities
{
  /// <summary>
  /// This entity maps to 'FormInfo'.
  /// </summary>
  public partial class FormInfo
  { 
    /// <summary>
    /// The default constructor for FormInfo class.
    /// </summary>
    public FormInfo()
    {
      
      
      
      this.Init();
    }

    partial void Init();

    #region "Basic Columns"


    /// <summary>
    /// Property Id mapping to FormInfo.Id
    /// </summary>
    public virtual System.Guid Id { get; set; }
    
    /// <summary>
    /// Property SourceUri mapping to FormInfo.SourceUri
    /// </summary>
    public virtual string SourceUri { get; set; }
    
    /// <summary>
    /// Property CustomerId mapping to FormInfo.CustomerId
    /// </summary>
    public virtual System.Guid? CustomerId { get; set; }
    
    /// <summary>
    /// Property CustomerScope mapping to FormInfo.CustomerScope
    /// </summary>
    public virtual string CustomerScope { get; set; }
    
    /// <summary>
    /// Property ListId mapping to FormInfo.ListId
    /// </summary>
    public virtual System.Guid? ListId { get; set; }
    
    /// <summary>
    /// Property ContentTypeId mapping to FormInfo.ContentTypeId
    /// </summary>
    public virtual string ContentTypeId { get; set; }
    
    /// <summary>
    /// Property Contents mapping to FormInfo.Contents
    /// </summary>
    public virtual string Contents { get; set; }
        
    #endregion
    
    #region "Outgoing References"
    #endregion
    
    #region "Incoming References"
    #endregion
    

  }
}