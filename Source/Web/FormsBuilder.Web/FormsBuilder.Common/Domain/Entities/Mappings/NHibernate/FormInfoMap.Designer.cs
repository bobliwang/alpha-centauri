using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FluentNHibernate.Mapping;

namespace FormsBuilder.Common.Domain.Entities.Mappings.NHibernate
{
	/// <summary>
	/// The mapping configuration for FormInfo.
	/// </summary>
	public partial class FormInfoMap : ClassMap<FormInfo>
	{
		/// <summary>
		/// The constructor.
		/// </summary>
		public FormInfoMap()
    {
			this.Table("FormInfo");
			

			// primary key mapping
			this.MapId();
			
			// basic columns mapping
			this.MapBasicColumns();
			
		}
		
		/// <summary>
		/// Map the Primary Key.
		/// </summary>
		private void MapId()
		{

			this.Id(x => x.Id)
			    .Column("Id");	
					  
		}
		
		/// <summary>
		/// Map the Basic Columns.
		/// </summary>
		private void MapBasicColumns()
		{

       // SourceUri
		   this.Map(x => x.SourceUri).Column("SourceUri")
 .Length(int.MaxValue) ;

			 
       // CustomerId
		   this.Map(x => x.CustomerId).Column("CustomerId")
;

			 
       // CustomerScope
		   this.Map(x => x.CustomerScope).Column("CustomerScope")
 .Length(int.MaxValue) ;

			 
       // ListId
		   this.Map(x => x.ListId).Column("ListId")
;

			 
       // ContentTypeId
		   this.Map(x => x.ContentTypeId).Column("ContentTypeId")
;

			 
       // Contents
		   this.Map(x => x.Contents).Column("Contents")
 .Length(int.MaxValue) ;

			 		}

	}
}