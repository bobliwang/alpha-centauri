﻿using System;

namespace FormsBuilder.Common.Domain.Entities
{
    public partial class FormInfo
    {
        partial void Init()
        {
            this.Id = Guid.NewGuid();
        }

    }

    public enum SourceType
    {
        SharePoint,

        SqlDatabase,
    }
}