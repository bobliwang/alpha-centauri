﻿namespace FormsBuilder.Common.Domain
{
    public interface ICacheable
    {
        string CacheKey { get; }
    }
}