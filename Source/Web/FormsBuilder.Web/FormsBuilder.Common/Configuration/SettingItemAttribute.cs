﻿using System;

namespace FormsBuilder.Common.Configuration
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SettingItemAttribute : Attribute
    {
        public SettingItemAttribute()
        {
        }

        public SettingItemAttribute(string key)
        {
            this.Key = key;
        }

        public string Key { get; set; }
    }
}