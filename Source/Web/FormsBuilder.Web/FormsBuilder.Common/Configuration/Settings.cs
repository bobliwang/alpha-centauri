﻿namespace FormsBuilder.Common.Configuration
{
    public class Settings
    {   
        [SettingItem]
        public string SqlConnString { get; set; }

        [SettingItem]
        public string RedisConnectionString { get; set; }

        [SettingItem]
        public string SharepointHostUrl { get; set; }

        [SettingItem]
        public string SharepointSpoidcrl { get; set; }

        [SettingItem]
        public string ProductId { get; set; }

        [SettingItem]
        public string ClientId { get; set; }

        [SettingItem]
        public string ClientSecret { get; set; }

        [SettingItem]
        public string SecondaryClientSecret { get; set; }
    }

}