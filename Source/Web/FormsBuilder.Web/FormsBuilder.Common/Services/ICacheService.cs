﻿using System;

namespace FormsBuilder.Common.Services
{
    public interface ICacheService
    {
        object Get(string key);

        void Put(string key, object value, DateTime expireOn, TimeSpan slidingExpireTimespan);

        void ClearKey(string key);
    }
}