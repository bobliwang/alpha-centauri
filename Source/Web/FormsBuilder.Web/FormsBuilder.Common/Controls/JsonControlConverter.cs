﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FormsBuilder.Common.Controls
{
    public class JsonControlConverter : Newtonsoft.Json.Converters.CustomCreationConverter<Control>
    {
        public override Control Create(Type objectType)
        {
            return new Control();
        }

        public Control Create(Type objectType, JObject jObject)
        {
            var controlTypeStr = (string)jObject.Property("controlType") ?? (string)jObject.Property("ControlType");

            var controlType = (ControlType)Enum.Parse(typeof(ControlType), controlTypeStr);

            return this.GetControlInstanceByControlType(controlType);
        }

        public Control GetControlInstanceByControlType(ControlType controlType)
        {
            switch (controlType)
            {
                case ControlType.Unknown:
                    throw new JsonSerializationException("ControlType Unknown is not supported");
                case ControlType.Label:
                    return new Label();
                case ControlType.TextBox:
                    return new TextBox();
                case ControlType.Image:
                    return new Image();
                case ControlType.Peoplepicker:
                    return new PeoplePicker();
                case ControlType.Lookup:
                    return new ListLookup();
                case ControlType.Repeater:
                    return new Repeater();
                case ControlType.RichText:
                case ControlType.CheckBox:
                case ControlType.DropdownList:
                case ControlType.List:
                case ControlType.DateTimePicker:
                case ControlType.FileUpload:
                case ControlType.CanvasColumnContainer:

                default:
                    return new Control();
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Load JObject from stream
            JObject jObject = JObject.Load(reader);

            // Create target object based on JObject
            var target = this.Create(objectType, jObject);

            // Populate the object properties
            serializer.Populate(jObject.CreateReader(), target);

            return target;
        }
    }
}