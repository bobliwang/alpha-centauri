﻿namespace FormsBuilder.Common.Controls
{
    public class Image : Control
    {
        [ControlSetting("Image Url")]
        public string ImageUrl { get; set; }
    }
}