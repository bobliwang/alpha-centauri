using System.Collections.Generic;
using Newtonsoft.Json;

namespace FormsBuilder.Common.Controls
{
    public class ControlSettingCategory
    {
        public ControlSettingCategory() : this(string.Empty)
        {
        }

        public ControlSettingCategory(string categoryName)
        {
            this.CategoryName = categoryName;
            this.ControlSettings = new List<ControlSetting>();
        }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }

        [JsonProperty("controlSettings")]
        public IList<ControlSetting> ControlSettings { get; set; }
    }
}