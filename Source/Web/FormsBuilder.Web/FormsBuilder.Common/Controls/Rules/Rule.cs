﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace FormsBuilder.Common.Controls.Rules
{
    public enum RuleType
    {
        Validation,

        Formatting,

        Hide
    }

    public interface IRule
    {   
        RuleType RuleType { get; }

        string Expression { get; }
    }

    public class Rule : IRule
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public RuleType RuleType { get; set; }

        public string Expression { get; set; }
    }

    public class RuleArray
    {
        public RuleArray()
        {
            this.Rules = new List<Rule>();
        }

        [JsonProperty("rules")]
        public IList<Rule> Rules { get; set; }
    }
}