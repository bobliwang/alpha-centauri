﻿namespace FormsBuilder.Common.Controls
{
    public class Label : Control
    {
        [ControlSetting("Text", "General")]
        public string Text { get; set; }

        [ControlSetting("Type", "General")]
        public override ControlType ControlType
        {
            get { return ControlType.Label; }
        }
    }
}