using System.ComponentModel;

namespace FormsBuilder.Common.Controls
{
    public enum ControlSettingCategoryNames
    {
        [Description("General Settings")]
        General,

        [Description("Validation Settings")]
        Validation,

        [Description("Advanced Settings")]
        Advanced
    }
}