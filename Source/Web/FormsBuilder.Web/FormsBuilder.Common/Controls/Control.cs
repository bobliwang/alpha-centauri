﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FormsBuilder.Common.Controls.Rules;
using FormsBuilder.Common.Domain.Metadata;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace FormsBuilder.Common.Controls
{
    public class Control
    {
        public Control()
        {
            this.ControlType = ControlType.Unknown;
            this.Columns = new List<Control>();
            this.RuleArray = new RuleArray();
        }

        [ControlSetting("Control Id", "General")]
        public Guid Id { get; set; }

        [ControlSetting("Control Name", "General")]
        public string Name { get; set; }
        
        public string DisplayName { get; set; }

        [ControlSetting("Columns", "Layout")]
        public int LayoutColumns { get; set; }

        [ControlSetting("Required", "Validation")]
        public bool Required { get; set; }

        public object Value { get; set; }
        
        public object DefaultValue { get; set; }

        [ControlSetting("Default Value", "General")]
        public string DefaultValueExpr { get; set; }

        [ControlSetting("Control Type", "General")]
        [JsonConverter(typeof(StringEnumConverter))]
        public virtual ControlType ControlType { get; set; }

        public IList<Control> Columns { get; set; }

        [ControlSetting("Rules", "Advanced")]
        public RuleArray RuleArray { get; set; }
    }
}