using FormsBuilder.Common.Domain.Metadata;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace FormsBuilder.Common.Controls
{
    public class ControlSetting
    {
        [JsonProperty("clientName")]
        public string ClientName
        {
            get { return this.Name.Substring(0, 1).ToLowerInvariant() + this.Name.Substring(1); }
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("dataType")]
        [JsonConverter(typeof(StringEnumConverter))]
        public DataType DataType { get; set; }
    }
}