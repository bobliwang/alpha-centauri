﻿namespace FormsBuilder.Common.Controls
{
    public class PeoplePicker : Control
    {
        [ControlSetting("Allow Multiple", "Advanced")]
        public bool IsMultiple { get; set; }
         
    }
}