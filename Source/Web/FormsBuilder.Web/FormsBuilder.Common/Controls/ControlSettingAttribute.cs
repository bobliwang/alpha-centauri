using System;
using System.Reflection;

namespace FormsBuilder.Common.Controls
{
    public class ControlSettingAttribute : Attribute
    {
        private static DataTypeManager dataTypeManager = new DataTypeManager();

        public ControlSettingAttribute(string displayName) : this(displayName, "General")
        {
        }

        public ControlSettingAttribute(string displayName, string categoryName)
        {
            this.ControlSetting = new ControlSetting
            {
                DisplayName = displayName,
                CategoryName = categoryName
            };
        }

        public ControlSetting ControlSetting { get; set; }

        public void ForPropertyInfo(PropertyInfo propertyInfo)
        {
            this.ControlSetting.Name = propertyInfo.Name;
            this.ControlSetting.DataType = dataTypeManager.DataTypeMaps[propertyInfo.PropertyType];
        }
    }
}