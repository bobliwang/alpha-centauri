﻿namespace FormsBuilder.Common.Controls
{
    public class TextBox : Control
    {
        [ControlSetting("Allow Edit", "Advanced")]
        public bool Editable { get; set; }

        public override ControlType ControlType
        {
            get { return ControlType.TextBox; }
        }
    }
}