﻿using FormsBuilder.Common.Domain.Form;
using Newtonsoft.Json;

namespace FormsBuilder.Common.Controls
{
    public class Repeater : Control
    {
        public Repeater()
        {
            this.TemplateRow = new RowControl();
        }

        [JsonProperty("templateRow")]
        public RowControl TemplateRow { get; set; }

        [JsonProperty("initialRows")]
        [ControlSetting("Inital Rows", "Advanced")]
        public int InitialRows { get; set; }
    }
}