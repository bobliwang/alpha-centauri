﻿using System;
using System.Collections.Generic;
using System.Linq;
using FormsBuilder.Common.Controls.Rules;
using FormsBuilder.Common.Domain.Metadata;

namespace FormsBuilder.Common.Controls
{
    public class DataTypeManager
    {
        private IDictionary<Type, DataType> _map;

        private IDictionary<DataType, ControlType> _dataTypeToControlTypeMap;

        public DataTypeManager()
        {
            _map = new Dictionary<Type, DataType>();

            _map[typeof(string)] = DataType.String;
            _map[typeof(short)] = DataType.Integer;
            _map[typeof(int)] = DataType.Integer;
            _map[typeof(long)] = DataType.Integer;
            _map[typeof(DateTime)] = DataType.DateTime;
            _map[typeof(decimal)] = DataType.DateTime;
            _map[typeof(double)] = DataType.Double;
            _map[typeof(float)] = DataType.Float;
            _map[typeof(Guid)] = DataType.Guid;
            _map[typeof(bool)] = DataType.Boolean;
            _map[typeof(byte[])] = DataType.Binary;

            _map[typeof(ControlType)] = DataType.String;
            _map[typeof(object)] = DataType.String;

            _map[typeof(RuleArray)] = DataType.Rules;


            _dataTypeToControlTypeMap = new Dictionary<DataType, ControlType>
            {
                  { DataType.Boolean               , ControlType.CheckBox              }
                , { DataType.DateTime              , ControlType.DateTimePicker        }
                , { DataType.String                , ControlType.TextBox               }
                , { DataType.Guid                  , ControlType.TextBox               }
                , { DataType.Integer               , ControlType.TextBox               }
                , { DataType.Double                , ControlType.TextBox               }
                , { DataType.Label                 , ControlType.Label                 }
                , { DataType.Binary                , ControlType.FileUpload            }
                , { DataType.Attachments           , ControlType.FileUpload            }
                , { DataType.CanvasColumnContainer , ControlType.CanvasColumnContainer }
                , { DataType.CanvasRow             , ControlType.CanvasRow             }
                , { DataType.Repeater              , ControlType.Repeater              }
                , { DataType.Image                 , ControlType.Image                 }
                , { DataType.People                , ControlType.Peoplepicker          }
                , { DataType.Lookup                , ControlType.Lookup                }
            };
        }

        public IDictionary<DataType, ControlType> DataTypeToControlTypeMap
        {
            get { return _dataTypeToControlTypeMap; }
        }

        public IDictionary<Type, DataType> DataTypeMaps
        {
            get
            {
                return _map;
            }
        }
        
        public ControlType MapToControlType(DataType dataType)
        {
            return _dataTypeToControlTypeMap[dataType];
        }

        public ControlSettingsRepository GetSettingsRepositoryForType(ControlType controlType)
        {
            var type = new JsonControlConverter().GetControlInstanceByControlType(controlType).GetType();

            return this.GetSettingsRepositoryForType(type);
        }

        public ControlSettingsRepository GetSettingsRepositoryForType(Type type)
        {
            var repo = new ControlSettingsRepository();

            var attrs = new List<ControlSettingAttribute>();

            foreach (var prop in type.GetProperties())
            {
                var attr = prop.GetCustomAttributes(true).OfType<ControlSettingAttribute>().FirstOrDefault();
                if (attr != null)
                {
                    attr.ForPropertyInfo(prop);

                    attrs.Add(attr);
                }
            }

            foreach (var grp in attrs.Select(x => x.ControlSetting).GroupBy(x => x.CategoryName))
            {
                repo.Categories.Add(new ControlSettingCategory
                {
                    CategoryName = grp.Key,
                    ControlSettings = grp.ToList()
                });
            }

            return repo;
        }
    }
}