﻿namespace FormsBuilder.Common.Controls
{
    public enum ControlType
    {
        Unknown = 0,

        Label,

        TextBox,
        RichText,

        CheckBox,
        DropdownList,
        List,

        DateTimePicker,
        FileUpload,

        CanvasColumnContainer,

        CanvasRow,

        Repeater,

        Peoplepicker,
        
        Image,

        Lookup,
        
    }
}