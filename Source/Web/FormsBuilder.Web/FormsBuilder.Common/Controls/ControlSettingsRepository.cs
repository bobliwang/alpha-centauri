using System.Collections.Generic;
using Newtonsoft.Json;

namespace FormsBuilder.Common.Controls
{
    public class ControlSettingsRepository
    {
        public ControlSettingsRepository()
        {
            this.Categories = new List<ControlSettingCategory>();
        }

        [JsonProperty("categories")]
        public IList<ControlSettingCategory> Categories { get; set; }
    }
}