﻿namespace FormsBuilder.Common.Controls
{
    public class ListLookup : Control
    {
        [ControlSetting("Source List Name", "Advanced")]
        public string SourceListName { get; set; }
        
        [ControlSetting("Filter by", "Advanced")]
        public string FilterField { get; set; }

        [ControlSetting("Filter value", "Advanced")]
        public object FilterValue { get; set; }

        [ControlSetting("Source List Id", "Advanced")]
        public string SourceListId { get; set; }
    }
}