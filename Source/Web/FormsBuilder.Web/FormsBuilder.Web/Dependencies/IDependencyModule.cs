﻿using System;
using System.Web;
using AlphaCentauriForms.SharePointAuth;
using Autofac;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FormsBuilder.Common.Configuration;
using FormsBuilder.Common.Domain;
using FormsBuilder.Common.Domain.Entities;
using FormsBuilder.Common.Domain.Metadata;
using FormsBuilder.Common.Domain.Repositories;
using FormsBuilder.Common.Domain.Repositories.NHibernate;
using FormsBuilder.Common.Services;
using FormsBuilder.Web.Configuration;
using FormsBuilder.Web.Services;
using NHibernate.Dialect;
using SharePointO365.RestClient;
using SharePointO365.RestClient.Auth;

namespace FormsBuilder.Web.Dependencies
{
    using StackExchange.Redis;

    public interface IDependencyModule
    {
        void RegisterDependencies(ContainerBuilder containerBuilder);
    }

    public class WebApiDependencyModule : IDependencyModule
    {
        public void RegisterDependencies(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<ConfigProvider>().As<IConfigProvider>().SingleInstance();
            containerBuilder.RegisterType<AppSettings>().As<Settings>().SingleInstance();
            
            containerBuilder.Register(c =>
            {
                var settings = c.Resolve<Settings>();
                var configOptions = ConfigurationOptions.Parse(settings.RedisConnectionString);
                var conn = ConnectionMultiplexer.Connect(configOptions);
                return new RedisCacheService(conn);

            }).As<ICacheService>().SingleInstance();

            containerBuilder.Register(c =>
            {
                var settings = c.Resolve<Settings>();
                return this.CreateSessionFactory(settings.SqlConnString);
            }).As<NHibernate.ISessionFactory>().SingleInstance();

            containerBuilder.Register(c =>
            {
                var sessionFac = c.Resolve<NHibernate.ISessionFactory>();
                var session = sessionFac.OpenSession();
                return new FormRepository
                {
                    CurrentSession = session, CurrentUserId = 0
                };

            }).As<IFormRepository>().InstancePerRequest();

            containerBuilder.RegisterType<TypedRedisCacheService<EntityMetadata>>()
                            .As<ITypedRedisCacheService<EntityMetadata>>()
                            .SingleInstance();

            containerBuilder.RegisterType<TypedRedisCacheService<CachedSharePointAcsContextInfo>>()
                           .As<ITypedRedisCacheService<CachedSharePointAcsContextInfo>>()
                           .SingleInstance();

            containerBuilder.Register(c =>
            {
                var settings = c.Resolve<Settings>();
                return this.GetSpoRestClient(settings, c);
            }).As<SpoRestClient>().InstancePerRequest();

            containerBuilder.RegisterInstance(new TypeMappingManager());

            containerBuilder.RegisterType<FormService>().As<IFormService>().InstancePerRequest();
            containerBuilder.RegisterType<WebService>().As<IWebService>().InstancePerRequest();
            containerBuilder.RegisterType<ListService>().As<IListService>().InstancePerRequest();

            containerBuilder.RegisterType<RedisSharePointAcsContextProvider>()
                .As<SharePointContextProvider>()
                .InstancePerRequest();

            containerBuilder.Register(c =>
            {
                var sharePointContextProvider = c.Resolve<SharePointContextProvider>();
                var spContext = sharePointContextProvider.GetSharePointContext(new HttpContextWrapper(HttpContext.Current));

                return spContext;
            }).As<SharePointContext>().InstancePerRequest();
        }

        private IAuthProvider GetAuthProvider(Settings settings, IComponentContext context)
        {
            var sharePointContextProvider = context.Resolve<SharePointContextProvider>();
            var sharepointContext = sharePointContextProvider.GetSharePointContext(HttpContext.Current);
            var authProvider = new AccessTokenAuthProvider(sharepointContext.UserAccessTokenForSPHost);

            return authProvider;
        }

        private SpoRestClient GetSpoRestClient(Settings settings, IComponentContext context)
        {
            var spContext = context.Resolve<SharePointContext>();
            var spHostUrl = spContext.SPHostUrl.ToString();

            if (spHostUrl.EndsWith("/"))
            {
                spHostUrl = spHostUrl.Substring(0, spHostUrl.Length - 1);
            }

            var authProvider = this.GetAuthProvider(settings, context);


            var spoClient = new SpoRestClient(spHostUrl, authProvider);

            return spoClient;
        }

        private NHibernate.ISessionFactory CreateSessionFactory(string sqlConnectionString)
        {
            var sessiongFactory = Fluently.Configure().Database(MsSqlConfiguration.MsSql2008.ConnectionString(sqlConnectionString).ShowSql()
                                .MaxFetchDepth(3)
                                .Dialect<MsSql2008Dialect>())
                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<FormInfo>()).BuildSessionFactory();

            return sessiongFactory;
        }
    }
}