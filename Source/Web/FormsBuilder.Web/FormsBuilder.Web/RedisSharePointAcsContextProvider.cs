﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using AlphaCentauriForms.SharePointAuth;
using FormsBuilder.Web.Services;

namespace FormsBuilder.Web
{
    using Common.Domain;

    public class RedisSharePointAcsContextProvider : SharePointAcsContextProvider
    {
        public RedisSharePointAcsContextProvider(ITypedRedisCacheService<CachedSharePointAcsContextInfo> cacheService)
        {
            this.CacheService = cacheService;
        }

        private ITypedRedisCacheService<CachedSharePointAcsContextInfo> CacheService { get; set; }

        protected override SharePointContext LoadSharePointContext(HttpContextBase httpContext)
        {
            var cookie = httpContext.Request.Cookies[SPCacheKeyKey];

            if (cookie == null)
            {
                return null;
            }

            var cacheKey = cookie.Value;
            var info = this.CacheService.Get(cacheKey);

            if (info == null)
            {
                return null;
            }

            try
            {
                SharePointContextToken contextToken = TokenHelper.ReadAndValidateContextToken(info.ContextToken, httpContext.Request.Url.Authority);
                var context = new SharePointAcsContext(
                    spHostUrl: info.GetSpHostUrlAsUri(),
                    spAppWebUrl: info.GetSpAppWebUrlAsUri(),
                    spLanguage: info.SpLanguage, 
                    spClientTag: info.SpClientTag,
                    spProductNumber: info.SpProductNumber,
                    contextToken: info.ContextToken,
                    contextTokenObj: contextToken,
                    userAccessTokenForSPHost: info.UserAccessTokenForSPHost,
                    userAccessTokenForSPAppWeb: info.UserAccessTokenForSPAppWeb,
                    appOnlyAccessTokenForSPHost: info.AppOnlyAccessTokenForSPHost,
                    appOnlyAccessTokenForSPAppWeb: info.AppOnlyAccessTokenForSPAppWeb
                );

                return context;
            }
            catch (Exception)
            {
                return null;
            }
        }

        protected override void SaveSharePointContext(SharePointContext spContext, HttpContextBase httpContext)
        {
            SharePointAcsContext spAcsContext = spContext as SharePointAcsContext;

            if (spAcsContext != null)
            {
                HttpCookie spCacheKeyCookie = new HttpCookie(SPCacheKeyKey)
                {
                    Value = spAcsContext.CacheKey,
                    Secure = true,
                    HttpOnly = true
                };

                httpContext.Response.AppendCookie(spCacheKeyCookie);
            }

            var contextToken = TokenHelper.ReadSharePointContextToken(spAcsContext.ContextToken).SharePointContextToken;

            var info = new CachedSharePointAcsContextInfo(spAcsContext.CacheKey)
            {
                ContextToken = spAcsContext.ContextToken,
                SpAppWebUrl = spAcsContext.SPAppWebUrl != null ? spAcsContext.SPAppWebUrl.ToString() : "",
                SpClientTag = spAcsContext.SPClientTag,
                SpHostUrl = spAcsContext.SPHostUrl.ToString(),
                SpLanguage = spAcsContext.SPLanguage,
                SpProductNumber = spAcsContext.SPProductNumber,
                RefreshToken = spAcsContext.RefreshToken,
                Realm = contextToken.Realm,
                UserAccessTokenForSPHost = spAcsContext.LoadUserAccessTokenForSPHost(),
                UserAccessTokenForSPAppWeb = spAcsContext.LoadUserAccessTokenForSPAppWeb(),
                AppOnlyAccessTokenForSPHost = spAcsContext.LoadAppOnlyAccessTokenForSPHost(),
                AppOnlyAccessTokenForSPAppWeb = spAcsContext.LoadAppOnlyAccessTokenForSPAppWeb()
            };

            this.CacheService.Put(info);
        }
    }

    public class CachedSharePointAcsContextInfo : ICacheable
    {
        public CachedSharePointAcsContextInfo(string cacheKey)
        {
            this.CacheKey = cacheKey;
        }

        public string CacheKey { get; set; }

        public string SpHostUrl { get; set; }

        public string SpAppWebUrl { get; set; }

        public string SpLanguage { get; set; }

        public string SpClientTag { get; set; }

        public string SpProductNumber { get; set; }

        public string ContextToken { get; set; }

        public string RefreshToken { get; set; }

        public string Realm { get; set; }

        public string TargetPrincipalName { get; set; }


        public TokenInfo UserAccessTokenForSPHost { get; set; }
        public TokenInfo UserAccessTokenForSPAppWeb { get; set; }
        public TokenInfo AppOnlyAccessTokenForSPHost { get; set; }
        public TokenInfo AppOnlyAccessTokenForSPAppWeb { get; set; }


        public Uri GetSpHostUrlAsUri()
        {
            return new Uri(this.SpHostUrl);
        }

        public Uri GetSpAppWebUrlAsUri()
        {
            if (string.IsNullOrEmpty(this.SpAppWebUrl))
            {
                return null;
            }

            return new Uri(this.SpAppWebUrl);
        }

    }
}