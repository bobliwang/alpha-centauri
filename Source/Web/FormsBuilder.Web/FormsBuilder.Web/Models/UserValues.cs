﻿using System.Collections.Generic;

namespace FormsBuilder.Web.Models
{
    public class UserValue
    {
        public int Id { get; set; }

        public int Title { get; set; }

        public int Email { get; set; }

        public int LoginName { get; set; }
    }
}