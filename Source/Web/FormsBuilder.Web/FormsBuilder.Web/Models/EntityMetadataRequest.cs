﻿using Newtonsoft.Json;

namespace FormsBuilder.Web.Models
{
    public class EntityMetadataRequest
    {
        [JsonProperty("connectionString")]
        public string ConnectionString { get; set; }

        [JsonProperty("entityName")]
        public string EntityName { get; set; }

        [JsonProperty("clearCache")]
        public bool ClearCache { get; set; } 
    }
}