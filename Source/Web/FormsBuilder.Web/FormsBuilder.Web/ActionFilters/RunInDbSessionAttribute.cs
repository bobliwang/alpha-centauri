﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using FormsBuilder.Common.Domain.Repositories;
using log4net;

namespace FormsBuilder.Web.ActionFilters
{
    public class RunInDbSessionAttribute : ActionFilterAttribute
    {
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool UseTransaction { get; set; }


        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            var repo = this.GetRepository(actionContext.Request);

            if (repo == null)
            {
                Logger.Warn("OnActionExecuting: repo == null -> true");
                return;
            }

            if (this.UseTransaction)
            {
                repo.BeginTransaction();
            }
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);

            var repo = this.GetRepository(actionExecutedContext.Request);
            if (repo == null)
            {
                Logger.Warn("OnActionExecuted: repo == null -> true");
                return;
            }


            if (this.CheckException(actionExecutedContext))
            {
                try
                {
                    if (this.UseTransaction)
                    {
                        repo.Commit();    
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message, ex);
                    throw;
                }
            }
            else
            {
                if (this.UseTransaction)
                {
                    repo.Rollback();
                }
            }
        }

        private bool CheckException(HttpActionExecutedContext actionExecutedContext)
        {
            var exception = actionExecutedContext.Exception;
            if (exception != null)
            {
                var msg = string.Format("OnActionExecuted: actionExecutedContext.Exception is found: {0}. Skip reosRepositories.Commit().", exception.Message);

                Logger.Error(msg);

                return false;
            }

            return true;
        }

        private IFormRepository GetRepository(HttpRequestMessage request)
        {
            return request.GetDependencyScope().GetService(typeof (IFormRepository)) as IFormRepository;
        }
    }
}