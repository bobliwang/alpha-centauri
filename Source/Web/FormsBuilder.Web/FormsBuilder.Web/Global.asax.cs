﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.WebApi;
using FormsBuilder.Common.Controls;
using FormsBuilder.Web.Dependencies;
using FormsBuilder.Web.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace FormsBuilder.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            this.ConfigDependencies();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            BundleConfig.RegisterDesignerBundles(BundleTable.Bundles);

            this.SetupJsonSerializationSettings(GlobalConfiguration.Configuration);
        }

        private void SetupJsonSerializationSettings(HttpConfiguration httpConfiguration)
        {
            var formatters = httpConfiguration.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            settings.Converters.Add(new JsonControlConverter());

            JsonConvert.DefaultSettings = () =>
            {
                var settings1 = new JsonSerializerSettings();

                settings1.Converters.Add(new JsonControlConverter());
                return settings1;
            };
        }

        protected void ConfigDependencies()
        {
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);

            builder.RegisterDependencies(new WebApiDependencyModule());

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
