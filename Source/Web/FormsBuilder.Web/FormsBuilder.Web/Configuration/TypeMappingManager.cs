﻿using System;
using System.Collections.Generic;
using System.Linq;
using FormsBuilder.Common.Controls;
using FormsBuilder.Common.Domain.Metadata;
using SharePointO365.RestClient.Enums;

namespace FormsBuilder.Web.Configuration
{
    public class TypeMappingManager : DataTypeManager
    {
        public FieldType[] SupportedFieldTypes
        {
            get
            {
                return new[]
                {
                    FieldType.Text,
                    FieldType.Boolean,
                    FieldType.Integer,
                    FieldType.Attachments,
                    FieldType.DateTime,
                    FieldType.User,
                    FieldType.Lookup
                };
            }
        }
        
        public DataType MapToDataType(FieldType fieldType)
        {
            switch (fieldType)
            {
                case FieldType.Invalid:
                    break;
                case FieldType.Integer:
                    return DataType.Integer;
                case FieldType.Text:
                    return DataType.String;
                case FieldType.Note:
                    break;
                case FieldType.DateTime:
                    return DataType.DateTime;
                case FieldType.Counter:
                    break;
                case FieldType.Choice:
                    break;
                case FieldType.Lookup:
                    return DataType.Lookup;
                case FieldType.Boolean:
                    return DataType.Boolean;
                case FieldType.Number:
                    return DataType.Double;
                case FieldType.Currency:
                    return DataType.Double;
                case FieldType.URL:
                    break;
                case FieldType.Computed:
                    break;
                case FieldType.Threading:
                    break;
                case FieldType.Guid:
                    return DataType.Guid;
                case FieldType.MultiChoice:
                    break;
                case FieldType.GridChoice:
                    break;
                case FieldType.Calculated:
                    break;
                case FieldType.File:
                    break;
                case FieldType.Attachments:
                    return DataType.Binary;
                case FieldType.User:
                    return DataType.People;
                case FieldType.Recurrence:
                    break;
                case FieldType.CrossProjectLink:
                    break;
                case FieldType.ModStat:
                    break;
                case FieldType.Error:
                    break;
                case FieldType.ContentTypeId:
                    break;
                case FieldType.PageSeparator:
                    break;
                case FieldType.ThreadIndex:
                    break;
                case FieldType.WorkflowStatus:
                    break;
                case FieldType.AllDayEvent:
                    break;
                case FieldType.WorkflowEventType:
                    break;
                default:
                    break;
                //throw new ArgumentOutOfRangeException("fieldType", fieldType, null);
            }

            return DataType.String;
        }
    }
}