﻿using System;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using FormsBuilder.Common.Configuration;
using FormsBuilder.Web.Extensions;
using FormsBuilder.Web.Services;

namespace FormsBuilder.Web.Configuration
{
    public class AppSettings : Settings
    {
        public AppSettings(IConfigProvider configProvider)
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                var attr = prop.GetCustomAttributes(true).OfType<SettingItemAttribute>().FirstOrDefault();
                if (attr != null)
                {
                    if (string.IsNullOrEmpty(attr.Key))
                    {
                        attr.Key = prop.Name;
                    }

                    prop.SetValue(this, configProvider.Get(attr.Key));
                }
            }
        }
    }
}