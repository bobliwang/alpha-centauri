﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlphaCentauriForms.SharePointAuth;

namespace FormsBuilder.Web.Controllers
{
    public partial class HomeController : Controller
    {
        private SharePointContextProvider spContextProvider;

        public HomeController(SharePointContextProvider spContextProvider)
        {
            this.spContextProvider = spContextProvider;
        }

        public virtual ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpPost]
        public virtual ActionResult FormDesigner()
        {
            this.spContextProvider.GetSharePointContext(this.HttpContext);

            return this.Redirect(Links.app.modules.designer.designer_cshtml);
        }
    }
}
