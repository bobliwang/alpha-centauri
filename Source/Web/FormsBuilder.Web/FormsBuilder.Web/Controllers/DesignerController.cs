﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AlphaCentauriForms.SharePointAuth;
using FormsBuilder.Common.Configuration;
using FormsBuilder.Common.Controls;
using FormsBuilder.Common.Domain;
using FormsBuilder.Common.Domain.Entities;
using FormsBuilder.Common.Domain.Form;
using FormsBuilder.Common.Domain.Metadata;
using FormsBuilder.Common.Domain.Repositories;
using FormsBuilder.Web.ActionFilters;
using FormsBuilder.Web.Configuration;
using FormsBuilder.Web.Models;
using FormsBuilder.Web.Services;
using Newtonsoft.Json;
using SharePointO365.RestClient;
using SharePointO365.RestClient.Models;

namespace FormsBuilder.Web.Controllers
{
    [RoutePrefix("api/designer")]
    public class DesignerController : ApiController
    {
        private readonly SharePointContextProvider spContextProvider;

        private readonly IListService listService;
        
        private readonly IFormService formService;

        private readonly IWebService webService;

        public DesignerController(SharePointContextProvider spContextProvider, IFormService formService, IListService listService, IWebService webService)
        {
            this.spContextProvider = spContextProvider;
            this.formService = formService;
            this.listService = listService;
            this.webService = webService;
        }


        [Route("landing")]
        [HttpPost]
        public async Task<IHttpActionResult> Landing()
        {
            this.spContextProvider.GetSharePointContext(new HttpContextWrapper(HttpContext.Current));

            var sb = new StringBuilder(this.Request.RequestUri.GetLeftPart(UriPartial.Authority));
            sb.Append("/app/modules/designer/designer.cshtml");
            var qry = this.Request.RequestUri.Query;
            if (!string.IsNullOrEmpty(qry))
            {
                sb.Append(qry);
            }

            var result = this.Redirect(sb.ToString());

            return await Task.FromResult(result);
        }

        [Route("BuiltInToolboxGroups")]
        [HttpGet]
        public async Task<IList<EntityMetadata>> GetBuiltInToolboxGroups()
        {
            var groups = new List<EntityMetadata>();

            groups.Add(new EntityMetadata
            {
                Name = "General",
                DisplayName = "General",
                Fields = new List<FieldMetadata>
                {
                    new FieldMetadata
                    {
                        DataType = DataType.Label,
                        DisplayName = "Label",
                        Name = "Label",
                        LayoutColumns = 2
                    },

                    new FieldMetadata
                    {
                        DataType = DataType.Image,
                        DisplayName = "Image",
                        Name = "Image",
                        LayoutColumns = 4
                    },

                    new FieldMetadata
                    {
                        DataType = DataType.String,
                        DisplayName = "TextBox",
                        Name = "TextBox",
                        LayoutColumns = 4
                    }
                }
            });

            groups.Add(new EntityMetadata
            {
                Name = "Containers",
                DisplayName = "Containers",
                Fields = new List<FieldMetadata>
                {
                    new FieldMetadata
                    {
                        DataType = DataType.CanvasColumnContainer,
                        DisplayName = "Column Container",
                        Name = "Column Container",
                        LayoutColumns = 6
                    },

                    new FieldMetadata
                    {
                        DataType = DataType.CanvasRow,
                        DisplayName = "Row",
                        Name = "Row",
                    },

                     new FieldMetadata
                    {
                        DataType = DataType.Repeater,
                        DisplayName = "Repeater",
                        Name = "Repeater",
                        LayoutColumns = 6
                    },
                }
            });

            return await Task.FromResult(groups);
        }

        [Route("EntityMetadata")]
        [HttpPost]
        public async Task<EntityMetadata> BuildEntityMetadata(ListInfo listInfo)
        {
            return await this.listService.GetEntityMetadataFromSharePoint(listInfo);
        }

        [Route("Lists/{listId}")]
        [HttpGet]
        public async Task<ListInfo> GetListInfo(Guid listId)
        {
            return await this.listService.GetListInfo(listId);
        }

        [Route("Lists/{listId}/ContentTypes")]
        [HttpGet]
        public async Task<IList<ContentTypeInfo>> GetContentTypes(Guid listId)
        {
            return await this.listService.GetContentTypes(listId);
        }

        [Route("AvailableUsers")]
        [HttpGet]
        public async Task<IList<UserInfo>> GetAvailableUsers()
        {
            return await this.webService.GetAvailableUsers();
        }

        [Route("ListLookupItems/{listId}")]
        [HttpGet]
        public async Task<IList<ListItem>> GetListLookupItems(Guid listId)
        {
            return await this.listService.GetListData(listId.ToString());
        }

        [Route("Forms/{listId}")]
        [HttpGet]
        [RunInDbSession(UseTransaction = true)]
        public async Task<Form> GetForm(Guid listId)
        {
            return await this.formService.GetForm(listId);
        }

        [Route("CreateDefaultForm")]
        [HttpPost]
        public async Task<Form> CreateDefaultForm(ListInfo listInfo)
        {
            return await this.formService.CreateDefaultForm(listInfo);
        }

        [Route("PublishForm")]
        [HttpPost]
        [RunInDbSession(UseTransaction = true)]
        public async Task<Guid> PublishForm(Form form)
        {
            return await this.formService.PublishForm(form);
        }

        [Route("SaveForm")]
        [HttpPost]
        [RunInDbSession(UseTransaction = true)]
        public async Task<Guid> SaveForm(Form form)
        {
            return await this.formService.SaveForm(form);
        }

        [Route("DataMappings")]
        [HttpGet]
        public IHttpActionResult GetDataMappings()
        {
            var typeMappingManager = new TypeMappingManager();

            var mappings = typeMappingManager.DataTypeToControlTypeMap
                    .ToDictionary(pair => pair.Key.ToString(), pair => pair.Value.ToString());

            var controlSettingRepos = new Dictionary<string, ControlSettingsRepository>();

            var types = Enum.GetValues(typeof (ControlType));

            foreach (ControlType type in types)
            {
                if (type != ControlType.Unknown)
                {
                    controlSettingRepos[type.ToString()] = typeMappingManager.GetSettingsRepositoryForType(type);
                }
            }

            return this.Json(new { mappings, controlSettingRepos });
        }
    }
}