﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AlphaCentauriForms.SharePointAuth;
using FormsBuilder.Common.Configuration;
using FormsBuilder.Common.Controls;
using FormsBuilder.Common.Domain;
using FormsBuilder.Common.Domain.Entities;
using FormsBuilder.Common.Domain.Form;
using FormsBuilder.Common.Domain.Metadata;
using FormsBuilder.Common.Domain.Repositories;
using FormsBuilder.Web.ActionFilters;
using FormsBuilder.Web.Configuration;
using FormsBuilder.Web.Models;
using FormsBuilder.Web.Services;
using Newtonsoft.Json;
using SharePointO365.RestClient;
using SharePointO365.RestClient.Auth;
using SharePointO365.RestClient.Enums;
using SharePointO365.RestClient.Models;

namespace FormsBuilder.Web.Controllers
{
    using Newtonsoft.Json.Linq;

    [RoutePrefix("api/runtime")]
    public class RuntimeController : ApiController
    {
        private SharePointContextProvider spContextProvider;

        public RuntimeController(SpoRestClient spoRestClient, SharePointContextProvider spContextProvider)
        {
            this.SpoRestClient = spoRestClient;
            this.spContextProvider = spContextProvider;
        }

        public SpoRestClient SpoRestClient { get; private set; }

        

        [Route("landing")]
        [HttpPost]
        public async Task<IHttpActionResult> Landing()
        {

            this.spContextProvider.GetSharePointContext(new HttpContextWrapper(HttpContext.Current));

            var sb = new StringBuilder(this.Request.RequestUri.GetLeftPart(UriPartial.Authority));
            sb.Append("/app/modules/runtime/runtime.cshtml");
            var qry = this.Request.RequestUri.Query;

            if (!string.IsNullOrEmpty(qry))
            {
                sb.Append(qry);
            }

            var result = this.Redirect(sb.ToString());

            return await Task.FromResult(result);
        }
        
        [Route("SubmitItem")]
        [HttpPost]
        [RunInDbSession(UseTransaction = true)]
        public async Task<bool> SubmitItem(Form form)
        {
            var listInfo = await this.SpoRestClient.GetListInfo(form.ListId);
            var listItemData = new ListItemData(listInfo.ListItemEntityTypeFullName);

            foreach (var row in form.Rows)
            {
                foreach (var ctrl in row.Columns)
                {
                    if (ctrl.ControlType == ControlType.TextBox)
                    {
                        if (ctrl.Value != null)
                        {
                            listItemData[ctrl.Name] = ctrl.Value.ToString();
                        }
                    }
                    else if (ctrl.ControlType == ControlType.CheckBox)
                    {
                        if (ctrl.Value != null)
                        {
                            listItemData[ctrl.Name] = bool.Parse(ctrl.Value.ToString());
                        }
                    }
                    else if (ctrl.ControlType == ControlType.DateTimePicker)
                    {
                        if (ctrl.Value != null)
                        {
                            listItemData[ctrl.Name] = DateTime.Parse(ctrl.Value.ToString());
                        }
                    }
                    else if (ctrl.ControlType == ControlType.Peoplepicker)
                    {
                        if (ctrl.Value != null)
                        {
                            int val;
                            try
                            {
                                var users = JsonConvert.DeserializeObject<IList<UserInfo>>(ctrl.Value.ToString());

                                val = users.First().Id;
                            }
                            catch
                            {
                                var user = JsonConvert.DeserializeObject<UserInfo>(ctrl.Value.ToString());

                                val = user.Id;
                            }

                            //// http://sharepoint.stackexchange.com/questions/71827/rest-post-how-to-add-a-list-item-with-people-and-group-choice-and-url-f
                            //// You need to write the ID to the field name as shown below.
                            //// Example: Field name = EmployeeName
                            //// You must write the user ID to EmployeeNameId
                            listItemData[ctrl.Name + "Id"] = val;
                        }
                    }
                }
            }

            await this.SpoRestClient.AddListItem(form.ListId, listItemData);

            return true;
        }
    }
}