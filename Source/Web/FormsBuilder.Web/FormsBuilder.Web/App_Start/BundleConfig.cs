﻿using System.Web;
using System.Web.Optimization;

namespace FormsBuilder.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/app/bower_components/chosen/chosen.css",
                      "~/app/bower_components/angular-ui-switch/angular-ui-switch.css",
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }

        public static void RegisterDesignerBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/bower_components").Include(
                "~/app/bower_components/underscore/underscore.js",
                "~/app/bower_components/jquery/dist/jquery.js",
                "~/app/bower_components/bootstrap/dist/js/bootstrap.js",
                "~/app/bower_components/angular/angular.js",
                "~/app/bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
                "~/app/bower_components/angular-sanitize/angular-sanitize.js",
                "~/app/bower_components/angular-ui-switch/angular-ui-switch.js",
                "~/app/bower_components/jquery-ui/jquery-ui.js",
                "~/app/libs/angular-sortable.js",
                "~/app/bower_components/ngtoast/dist/ngToast.js",
                "~/app/bower_components/chosen/chosen.jquery.js",
                "~/app/bower_components/angular-chosen-localytics/dist/angular-chosen.js"
            ));

            bundles.Add(
                new ScriptBundle("~/bundles/designer").Include("~/app/modules/designer/index.js")
                    .IncludeDirectory("~/app/modules/services", "*.js", true)
                    .IncludeDirectory("~/app/modules/designer/components", "*.js", true)
                    .IncludeDirectory("~/app/modules/designer/controls", "*.js", true)
                    .IncludeDirectory("~/app/modules/designer/services", "*.js", true)
            );

            bundles.Add(
                new ScriptBundle("~/bundles/runtime").Include("~/app/modules/runtime/index.js")
                    .IncludeDirectory("~/app/modules/services", "*.js", true)
                    .IncludeDirectory("~/app/modules/runtime/controls", "*.js", true)
                    .IncludeDirectory("~/app/modules/runtime/services", "*.js", true)
            );
        }
    }
}
