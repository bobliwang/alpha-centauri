﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpressionExtensions.cs" company="Airloom">
//   Copyright © Airloom Dev Team. All Rights Reserved.
// </copyright>
// <summary>
//   Defines the ExpressionExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq.Expressions;
using System.Reflection;

namespace FormsBuilder.Web.Extensions
{
  /// <summary>
  /// The ExpressionExtensions class.
  /// </summary>
  public static class ExpressionExtensions
  {
    /// <summary>
    /// GetMemberExpression method.
    /// </summary>
    /// <param name="expression">
    /// The expression.
    /// </param>
    /// <typeparam name="T">
    /// The type T.
    /// </typeparam>
    /// <returns>
    /// A member expression.
    /// </returns>
    public static MemberExpression GetMemberExpression<T>(this Expression<Func<T, object>> expression)
    {
      return expression.GetMemberExpression<T, object>();
    }

    public static MemberExpression GetMemberExpression<T, P>(this Expression<Func<T, P>> expression)
    {
      var lambda = expression as LambdaExpression;
      if (lambda == null)
      {
        throw new NullReferenceException(@"lambda");
      }

      MemberExpression memberExpr = null;
      if (lambda.Body.NodeType == ExpressionType.Convert)
      {
        var unaryExpr = lambda.Body as UnaryExpression;
        if (unaryExpr == null)
        {
          throw new NullReferenceException(@"unaryExpr");
        }

        memberExpr = unaryExpr.Operand as MemberExpression;
      }
      else if (lambda.Body.NodeType == ExpressionType.MemberAccess)
      {
        memberExpr = lambda.Body as MemberExpression;
      }

      if (memberExpr == null)
      {
        throw new ArgumentException("Not a member expression.");
      }

      return memberExpr;
    }

    /// <summary>
    /// Get the property info.
    /// </summary>
    /// <param name="expression">
    /// The expression.
    /// </param>
    /// <typeparam name="T">
    /// The type T.
    /// </typeparam>
    /// <returns>
    /// The property info.
    /// </returns>
    public static PropertyInfo GetPropertyInfo<T>(this Expression<Func<T, object>> expression)
    {
      var memberExpr = expression.GetMemberExpression();
      return (PropertyInfo)memberExpr.Member;
    }

    /// <summary>
    /// Get the field info.
    /// </summary>
    /// <param name="expression">
    /// The expression.
    /// </param>
    /// <typeparam name="T">
    /// The type T.
    /// </typeparam>
    /// <returns>
    /// The field info.
    /// </returns>
    public static FieldInfo GetFieldInfo<T>(this Expression<Func<T, object>> expression)
    {
      var memberExpr = expression.GetMemberExpression();
      return (FieldInfo)memberExpr.Member;
    }

    /// <summary>
    /// Get the property info.
    /// </summary>
    /// <param name="expression">
    /// The expression.
    /// </param>
    /// <typeparam name="T">
    /// The type T.
    /// </typeparam>
    /// <typeparam name="P"></typeparam>
    /// <returns>
    /// The property info.
    /// </returns>
    public static PropertyInfo GetPropertyInfo<T, P>(this Expression<Func<T, P>> expression)
    {
      var memberExpr = expression.GetMemberExpression();
      return (PropertyInfo)memberExpr.Member;
    }

    /// <summary>
    /// Get the field info.
    /// </summary>
    /// <param name="expression">
    /// The expression.
    /// </param>
    /// <typeparam name="T">
    /// The type T.
    /// </typeparam>
    /// <typeparam name="P"></typeparam>
    /// <returns>
    /// The field info.
    /// </returns>
    public static FieldInfo GetFieldInfo<T, P>(this Expression<Func<T, P>> expression)
    {
      var memberExpr = expression.GetMemberExpression();
      return (FieldInfo)memberExpr.Member;
    }
  }
}
