﻿using FormsBuilder.Common.Services;
using Newtonsoft.Json;

namespace FormsBuilder.Web.Extensions
{
    public static class CacheServiceExtensions
    {
        public static T Get<T>(this ICacheService cacheService, string key)
        {
            var val = cacheService.Get(key);

            var stringVal = val as string;

            if (string.IsNullOrEmpty(stringVal))
            {
                return default(T);
            }

            if (typeof (T) == typeof (string))
            {
                return (T) val;
            }

            return JsonConvert.DeserializeObject<T>(stringVal);
        }
    }
}