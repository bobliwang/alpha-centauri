﻿using Autofac;
using FormsBuilder.Web.Dependencies;

namespace FormsBuilder.Web.Extensions
{
    public static class ContainerBuilderExtensions
    {
        public static void RegisterDependencies(this ContainerBuilder builder, IDependencyModule module)
        {
            module.RegisterDependencies(builder);
        }
    }
}