﻿$(function () {

    var clientId = '3806f331-0d2b-4cd7-ab42-fbb211c68ef0';

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var decodedUrl = decodeURIComponent(document.location.toString()).replace(/amp;/g, '');
        var results = regex.exec(decodedUrl);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }   
    }

    function replaceQueryString(url, param, value) {
        var re = new RegExp("([?|&])" + param + "=.*?(&|$)", "i");
        if (url.match(re)) {
            return url.replace(re, '$1' + param + "=" + value + '$2');
        } else {
            return url + '&' + param + "=" + value;
        }   
    }

    function getAppWebUrl() {
        var documentLocation = document.location.toString();
        var appWeb = '';

        var appName = "/AlphaCentauriFormsSharepointApp";
        if (documentLocation.indexOf(appName) > 0) {
            appWeb = documentLocation.substring(0, documentLocation.indexOf(appName)) + appName;
        }
        return appWeb;
    }

    function loadFormFillerframe() {
        var originalUrl = document.location.toString();
        var remoteAppUrl = getParameterByName("remoteAppUrl");
        var redirectUri = getParameterByName("redirectUri");
        
        var querystringposition = originalUrl.indexOf('?');
        var url = remoteAppUrl + '/api/runtime/landing';
        var appWebUrl = getAppWebUrl();

        var querystring = '';
        if (querystringposition > 0) {
            querystring = originalUrl.substr(querystringposition);
        }

        querystring = querystring.replace(/amp;/g, '');
        if (appWebUrl.length > 1) {
            querystring = replaceQueryString(querystring, "SPAppWebUrl", appWebUrl);
        }
        
        document.body.style.margin = 0;
        document.body.style.overflow = "auto";

        var frame = $('iframe')[0];
        frame.frameBorder = 0;
        frame.style.border = 0;
        frame.style.width = '100%';
        frame.style.height = '100%';
        frame.style.position = 'absolute';
        frame.style.top = 0;
        frame.style.left = 0;
        frame.style.right = 0;
        frame.style.bottom = 0;
        frame.target = "_top";
        frame.src = appWebUrl + '/_layouts/15/appredirect.aspx?client_id=' + clientId + '&redirect_uri=' + encodeURIComponent(url + querystring);
    }

    loadFormFillerframe();
});