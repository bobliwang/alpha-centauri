designerModule.factory('appStateManager', ["$log", "appState",
    function ($log, appState) {

        var self = this;

        self.appStateManager = self.appStateManager || {
            deleteRow: function(row) {
                self.appStateManager.selectRow(null);

                appState.form.rows = _.reject(appState.form.rows, function (r) {
                    return row === r;
                });
            },

            selectControl: function (cc) {

                if (cc == null) {
                    if (appState.selectedControl != null) {
                        appState.selectedControl.isSelected = false;
                    }

                    appState.selectedControl = null;
                    return;
                }

                var originalVal = cc.isSelected;

                if (appState.selectedControl) {
                    appState.selectedControl.isSelected = false;
                }

                cc.isSelected = !originalVal;

                appState.selectedControl = cc.isSelected ? cc : null;
            },


            selectRow: function (row) {

                if (row == null) {

                    if (appState.selectedRow != null) {
                        appState.selectedRow.isSelected = false;
                    }

                    appState.selectedRow = null;
                    return;
                }

                var originalVal = row.isSelected;

                if (appState.selectedRow) {
                    appState.selectedRow.isSelected = false;
                }

                row.isSelected = !originalVal;

                appState.selectedRow = row.isSelected ? row : null;
            },


            deleteControl: function (cc) {
                var row = _.find(appState.form.rows, function (r) {
                    return _.contains(r.columns, cc);
                });

                if (row) {

                    self.appStateManager.selectControl(null);

                    row.columns = _.reject(row.columns, function (c) {
                        return c === cc;
                    });
                }
            },

            resetForm: function () {

                var oldId = null;
                if (appState.form) {
                    oldId = appState.form.id;
                }

                appState.form = {
                    id: oldId,
                    rows: [
                        {
                            columns: []
                        }
                    ]
                };
            }
        };

        return self.appStateManager;
    }
]);