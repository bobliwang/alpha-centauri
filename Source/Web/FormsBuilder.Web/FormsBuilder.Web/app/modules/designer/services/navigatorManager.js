designerModule.factory('navigatorManager', ["$log", "appState",
    function ($log, appState) {

        var self = this;

        self.navigatorManager = self.navigatorManager || {
            goToSharePoint: function() {
                window.location.href = appState.src;
            }
        };

        return self.navigatorManager;
    }
]);