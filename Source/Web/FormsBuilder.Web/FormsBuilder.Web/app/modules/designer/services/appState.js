designerModule.factory('appState', ['$log', function ($log) {
    var self = this;

    self.appState = self.appState || {
        listId: ac.designer.globalSettings.ListId,
        src: ac.designer.globalSettings.Src,
        listInfo: {},
        form: null,
        selectedControl: null
    };

    return self.appState;
}]);
