var designerModule = angular.module("rf-designer",
    ['ui.bootstrap', 'ui.sortable', 'ngToast', 'rf-api', 'rf-utils', 'rf-dialogs', 'localytics.directives', 'uiSwitch']);

(function (designerModule) {

    designerModule.utils = designerModule.utils || { };
    designerModule.utils.controlTemplateUrl = function(controlName) {
        return '/app/modules/designer/controls/' + controlName + '.tpl.html';
    };

    
    designerModule.controller("FormBuilderController", ['$scope', '$log', '$http', 'designerApi', 'appState', 'appStateManager', 'dataTypeControlMappings',
        function ($scope, $log, $http, designerApi, appState, appStateManager, dataTypeControlMappings) {

            $scope.appState = appState;
            $scope.appStateManager = appStateManager;

            $scope.init = function () {
                $log.info("FormBulderController Init ..");

                designerApi.getDataMappings().then(function (response) {
                    dataTypeControlMappings.mappings = response.data.mappings;
                    dataTypeControlMappings.controlSettingRepos = response.data.controlSettingRepos;

                    $scope.isReady = true;
                });
            };


            $scope.init();
        }])
    ;
})(designerModule);
