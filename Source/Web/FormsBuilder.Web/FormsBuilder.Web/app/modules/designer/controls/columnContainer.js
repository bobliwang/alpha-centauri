(function (designerModule) {

    var controlName = "ColumnContainer";

    designerModule
        .directive('rfDesigner' + controlName, ['$log', '$timeout', 'appState', 'appStateManager', 'sortableOptionProvider',

        function ($log, $timeout, appState, appStateManager, sortableOptionProvider) {

            return {
                restrict: 'E',
                templateUrl: designerModule.utils.controlTemplateUrl(controlName),

                scope: {
                    control: "="
                },

                controller: function ($scope, $log, $timeout, appState, appStateManager) {
                    $scope.appState = appState;
                    $scope.appStateManager = appStateManager;

                    $scope.canvasControl_SortableOptions = sortableOptionProvider.getControlSortableOptions($scope);
                }
            };
        }])
    ;
})(designerModule);

