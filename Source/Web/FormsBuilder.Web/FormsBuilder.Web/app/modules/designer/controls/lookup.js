(function (designerModule) {

    var controlName = "Lookup";

    designerModule.directive('rfDesigner' + controlName, ['$log', '$timeout', 'appState', 'appStateManager', 'designerApi',
        function ($log, $timeout, appState, appStateManager, designerApi) {

            return {
                restrict: 'E',
                templateUrl: designerModule.utils.controlTemplateUrl(controlName),

                scope: {
                    control: "="
                },

                controller: function ($scope, $log, $timeout, appState, appStateManager) {
                    $scope.appState = appState;
                    $scope.appStateManager = appStateManager;
                }
            };
        }]);
})(designerModule);

