(function (designerModule) {
    
    var controlName = "Fileupload";

    designerModule
    .directive('rfDesigner' + controlName, ['$log', '$timeout', 'appState', 'appStateManager', function ($log, $timeout, appState, appStateManager) {

        return {
            restrict: 'E',
            templateUrl: designerModule.utils.controlTemplateUrl(controlName),

            scope: {
                control: "="
            },

            controller: function ($scope, $log, $timeout, appState, appStateManager) {
                $scope.appState = appState;
                $scope.appStateManager = appStateManager;
            }
        };
    }])
    ;
})(designerModule);