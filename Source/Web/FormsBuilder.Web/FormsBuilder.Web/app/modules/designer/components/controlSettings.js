(function (designerModule) {
    designerModule
        .directive('rfControlSettings', ['designerApi', '$uibModal', '$log', 'ngToast', 'resourceStrings',
            function (designerApi, $uibModal, $log, ngToast, resourceStrings) {
                return {
                    restrict: 'E',
                    templateUrl: '/app/modules/designer/components/controlSettings.tpl.html',

                    scope: {
                        control: '='
                    },

                    controller: ['$scope', '$location', '$log', '$timeout', 'appState', 'appStateManager', 'dataTypeControlMappings', 'dialogManager', function ($scope, $location, $log, $timeout, appState, appStateManager, dataTypeControlMappings, dialogManager) {

                        $scope.appState = appState;
                        $scope.appStateManager = appStateManager;
                        
                        $scope.$watch("control", function (newVal) {

                            if (newVal) {
                                $scope.controlSettingsRepo = dataTypeControlMappings.controlSettingRepos[newVal.controlType];
                            }
                        });

                        function ensureRules(control) {
                            control.ruleArray = control.ruleArray || { rules: [] };
                        }

                        $scope.deleteRule = function (rule) {
                            if ($scope.control) {
                                ensureRules($scope.control);
                                $scope.control.ruleArray.rules = _.reject($scope.control.ruleArray.rules, function (x) {
                                    return x === rule;
                                });
                            }
                        };

                        $scope.addNewRule = function() {
                            if ($scope.control) {
                                ensureRules($scope.control);
                                $scope.control.ruleArray.rules.push({});
                            }
                        };
                    }]
                };
        }]);
})(designerModule);
