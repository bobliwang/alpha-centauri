(function (designerModule) {
    designerModule.factory('sortableOptionProvider', ['$log', 'dataTypeControlMappings',
        function ($log, dataTypeControlMappings) {

            return {
                getRowSortableOptions: function ($scope) {
                    var savedRow = null;

                    return {
                        connectWith: ".canvasRows",

                        update: function (e, ui) {
                            if (!ui.item.sortable.isCanceled()) {
                                if (savedRow != null) {

                                    $scope.$apply(function () {
                                        $log.info("update dropindex:", ui.item.sortable.dropindex, savedRow);
                                        ui.item.sortable.received = true;
                                        ui.item.sortable.sourceModel.splice(ui.item.sortable.dropindex, 0, savedRow);
                                    });
                                }
                            }
                        },

                        receive: function (e, ui) {

                            $log.info('canvasControl_SortableOptions receive ..');

                            var newRow = ui.item.data('draggedData');
                            if (newRow) {
                                var copy = _.clone(newRow);
                                copy.$$hashKey = _.uniqueId('dnd_guid_row_');
                                copy.columns = [];
                                copy.rules = copy.rules || [];
                                savedRow = copy;
                            } else {
                                savedRow = null;
                            }

                        },

                        stop: function (e, ui) {
                            if (savedRow) {
                                savedRow = null;
                                ui.item.remove();
                            }
                        }
                    };
                },

                getControlSortableOptions: function ($scope) {

                    var savedToolboxItem;

                    return {
                        connectWith: ".canvasColumns",

                        update: function (e, ui) {

                            $log.info('canvasControl_SortableOptions update ..');

                            if (!ui.item.sortable.isCanceled()) {
                                if (savedToolboxItem != null) {

                                    $scope.$apply(function () {
                                        console.log("update dropindex:", ui.item.sortable.dropindex, savedToolboxItem);
                                        ui.item.sortable.received = true;
                                        ui.item.sortable.sourceModel.splice(ui.item.sortable.dropindex, 0, savedToolboxItem);
                                    });
                                }
                            }
                        },

                        receive: function (e, ui) {

                            $log.info('canvasControl_SortableOptions receive ..');

                            var toolboxItem = ui.item.data('draggedData');
                            if (toolboxItem) {

                                var control = _.clone(toolboxItem);
                                control.$$hashKey = _.uniqueId('dnd_guid_item_');
                                control.id = null;
                                control.controlType = dataTypeControlMappings.mappings[toolboxItem.dataTypeAsString];
                                control.ruleArray = control.ruleArray || { rules : []};

                                var controlTypesWithColumns = ["CanvasRow", "CanvasColumnContainer"];

                                if (controlTypesWithColumns.indexOf(control.controlType)) {
                                    control.columns = control.columns || [];
                                }

                                if (control.controlType === "Repeater") {
                                    control.templateRow = control.templateRow || {};
                                    control.templateRow.columns = control.templateRow.columns || [];
                                }


                                savedToolboxItem = control;
                            } else {
                                savedToolboxItem = null;
                            }
                        },

                        stop: function (e, ui) {

                            if (savedToolboxItem) {
                                savedToolboxItem = null;
                                ui.item.remove();
                            }

                        }
                    };
                }
            };

        }
    ]);
})(designerModule);
