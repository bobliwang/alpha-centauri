(function (designerModule) {
    designerModule.directive('rfControl', ['recursionHelper', 'sortableOptionProvider', 'dataTypeControlMappings',
        function (recursionHelper, sortableOptionProvider, dataTypeControlMappings) {
            return {
                restrict: 'E',
                templateUrl: '/app/modules/designer/components/control.tpl.html',

                scope: {
                    control: "="
                },

                compile: function (element) {
                    // Use the compile function from the RecursionHelper,
                    // And return the linking function(s) which it returns
                    return recursionHelper.compile(element);
                },

                controller: function ($scope, $log, $timeout, $element, appState, appStateManager) {

                    $scope.appState = appState;
                    $scope.appStateManager = appStateManager;

                    $scope.popoverTemplate = "/app/modules/designer/components/control.popover.html";

                    $scope.getControlType = function (dataTypeAsString) {
                        return dataTypeControlMappings.mappings[dataTypeAsString];
                    };
                                        
                    $scope.removeControl = function (cc) {

                        if (appState.selectedControl === cc) {
                            appState.selectedControl = null;
                        }

                        function findParent(item, parent) {
                            if (parent.columns && parent.columns.indexOf(item) > -1) {
                                return parent;
                            }

                            if (parent.columns) {
                                for (var i = 0; i < parent.columns.length; i++) {
                                    var result = findParent(item, parent.columns[i]);

                                    if (result) {
                                        return result;
                                    }
                                }
                            }

                            return null;
                        }

                        _.each(appState.form.rows, function (row) {
                            var parent = findParent(cc, row);

                            if (parent && parent.columns) {

                                var index = parent.columns.indexOf(cc);

                                parent.columns.splice(index, 1);
                            }
                        });
                    };

                    $scope.widen = function (cc) {
                        cc.layoutColumns++;
                    };

                    $scope.shorten = function (cc) {
                        cc.layoutColumns--;
                    };

                    $scope.alignInContainer = function (cc, alignVal) {
                        cc.container = cc.container || {};
                        cc.container.style = cc.container.style || {};
                        cc.container.style["text-align"] = alignVal;
                    }

                    $scope.canvasControl_SortableOptions = sortableOptionProvider.getControlSortableOptions($scope);

                    $scope.$watch("control.isSelected", function (isSelected) {
                        if (isSelected) {
                            // enable the keyup/keydown/keypress event
                            $element.focus();

                            $log.info("focused: ", $scope.control, $element);
                        }
                        else {
                            $element.blur();
                        }
                    });
                }
            };
    }]);
})(designerModule);
