(function(designerModule) {
    designerModule
        .directive('rfBanner', [function () {

            return {
                restrict: 'E',
                templateUrl: '/app/modules/designer/components/banner.tpl.html',

                scope: {
                },

                controller: ['$scope', '$log', 'appState', 'appStateManager', function ($scope, $log, appState, appStateManager) {

                    $scope.appState = appState;
                    $scope.appStateManager = appStateManager;
                }]
            };
        }])
    ;
})(designerModule);
