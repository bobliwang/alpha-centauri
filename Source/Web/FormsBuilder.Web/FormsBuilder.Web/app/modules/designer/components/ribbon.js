(function (designerModule) {

    designerModule.controller('ribbonController', [
        '$scope', '$location', '$log', '$timeout', 'appState', 'appStateManager', 'designerApi', 'navigatorManager', 'dialogManager', 'ngToast', '$uibModal', 'resourceStrings',
        function ($scope, $location, $log, $timeout, appState, appStateManager, designerApi, navigatorManager, dialogManager, ngToast, $uibModal, resourceStrings) {

            $scope.appState = appState;
            $scope.appStateManager = appStateManager;

            $scope.showDefaultForm = function() {
                designerApi.createDefaultForm(appState.listInfo).then(function(response) {

                    var oldForm = appState.form || {};

                    appState.form = response.data;
                    appState.form.id = oldForm.id;
                    appState.form.contentTypeId = oldForm.contentTypeId;
                });;
            };

            $scope.getPreviewFormUrl = function() {
                return "/app/modules/runtime/runtime.cshtml" + window.location.search;
            };

            $scope.previewForm = function() {

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'previewForm.html',
                    size: 'lg',
                    controller: [
                        "$scope", "$uibModalInstance", "previewFormUrl", function(scope, $uibModalInstance, previewFormUrl) {

                            scope.previewFormUrl = previewFormUrl;

                            scope.ok = function() {
                                $uibModalInstance.close();
                            };

                            scope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ],
                    resolve: {
                        previewFormUrl: function() {
                            return $scope.getPreviewFormUrl();
                        }
                    }
                });

                modalInstance.result.then(function(result) {
                    $log.info('Modal dismissed at: ' + new Date());
                }, function() {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };

            $scope.saveForm = function() {

                var info = ngToast.info({
                    content: "Saving form",
                    dismissOnTimeout: false
                });

                var contentType = _.findWhere(appState.listInfo.contentTypes, { isSelected: true });

                if (contentType) {
                    appState.form.contentTypeId = contentType.id.stringValue;
                }

                designerApi
                    .saveForm(appState.form)
                    .then(function(response) {
                        appState.form.Id = response.data;

                        ngToast.success({
                            content: resourceStrings.Msg_FormSaved
                        });

                        ngToast.dismiss(info);
                    });
            };

            $scope.getForm = function() {
                designerApi.getForm(appState.listId).then(function(response) {
                    appState.form = response.data;

                    ngToast.success({
                        content: resourceStrings.Msg_FormLoaded
                    });
                });
            };

            $scope.resetForm = function() {
                appStateManager.resetForm();
            };

            $scope.publishForm = function() {

                var contentType = _.findWhere(appState.listInfo.contentTypes, { isSelected: true });
                if (contentType) {
                    appState.form.contentTypeId = contentType.id.stringValue;
                }


                var info = ngToast.info({
                    content: "Publishing form",
                    dismissOnTimeout: false
                });


                designerApi
                    .publishForm(appState.form)
                    .then(function(response) {
                        appState.form.Id = response.data;

                        ngToast.success({
                            content: resourceStrings.Msg_FormSaved
                        });

                        ngToast.dismiss(info);
                    });
            };

            $scope.addRow = function() {
                appState.form.rows.push({
                    columns: []
                });
            };

            $scope.closeDesigner = function() {

                dialogManager.confirm('sure to quit?', 'quit').then(function(dlgResult) {
                    if (dlgResult.result) {
                        navigatorManager.goToSharePoint();
                    }
                });
            };
        }
    ]);

    designerModule.directive('rfRibbon', [
        function () {
            return {
                restrict: 'E',
                templateUrl: '/app/modules/designer/components/ribbon.tpl.html',

                scope: {
                },

                controller: 'ribbonController'
            };
        }]);
})(designerModule);
