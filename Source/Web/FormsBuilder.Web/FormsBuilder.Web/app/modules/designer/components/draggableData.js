(function (designerModule) {

    designerModule.directive('rfDraggableData',[function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                var draggedDataAttr = attrs["rfDraggableData"];
                element.data("draggedData", scope.$eval(draggedDataAttr));

                var connectToSortable = attrs["connectToSortable"];

                element.draggable({
                    connectToSortable: connectToSortable,
                    revert: true, // immediately snap back to original position
                    revertDuration: 0, //
                    helper: 'clone'
                });

                element.disableSelection();

            }
        };
    }]);

})(designerModule);
