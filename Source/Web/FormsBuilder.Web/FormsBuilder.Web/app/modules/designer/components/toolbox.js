(function (designerModule) {
    designerModule.directive('rfToolbox', function () {
        return {
            restrict: 'E',
            templateUrl: '/app/modules/designer/components/toolbox.tpl.html',

            scope: {
            },

            controller: function ($scope, $q, $log, designerApi, appState) {

                $scope.init = function () {

                    $scope.appState = appState;

                    designerApi.getListInfo(appState.listId)
                        .then(function (response) {
                            appState.listInfo = response.data;

                            var prom1 = designerApi.getBuiltInToolboxGroups();
                            var prom2 = designerApi.requestEntityMetadata(appState.listInfo);

                            $q.all([prom1, prom2]).then(function(responses) {
                                var groups = responses[0].data;

                                groups.push(responses[1].data);

                                appState.toolboxGroups = groups;
                            });
                        });
                };

                $scope.getConnectToSortable = function (fd) {
                    switch (fd.dataType) {
                        case 1001:
                            return '.canvasRows';

                        default:
                            return '.canvasColumns';
                    }
                };

                $scope.init();
            }
        };
    });

})(designerModule);
