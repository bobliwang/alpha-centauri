(function (designerModule) {
    designerModule.directive('rfContentTypePicker', ['designerApi', '$uibModal', '$log', 'ngToast',
        function (designerApi, $uibModal, $log, ngToast) {

            ngToast.settings.verticalPosition = 'bottom';
            ngToast.settings.timeout = 2000;

            return {
                restrict: 'E',
                templateUrl: '/app/modules/designer/components/contentTypePicker.tpl.html',

                scope: {
                    listId: "="
                },

                controller: ['$scope', '$log', '$timeout', 'designerApi', 'appState', 'appStateManager', 'navigatorManager', 'dialogManager',
                    function ($scope, $log, $timeout, designerApi, appState, appStateManager, navigatorManager, dialogManager) {
                        $scope.appState = appState;
                        $scope.appStateManager = appStateManager;

                        $scope.init = function () {

                            var dlg = $uibModal.open({
                                animation: true,
                                templateUrl: 'contentTypesPickerDlg.tpl.html',
                                size: 'lg',

                                controller: ["$scope", "$uibModalInstance", function (scope, $uibModalInstance) {

                                    scope.appState = appState;

                                    scope.selectContentType = function (contentType) {
                                        _.each(appState.listInfo.contentTypes, function (ct) {
                                            ct.isSelected = false;
                                        });

                                        contentType.isSelected = true;

                                        scope.ok();
                                    };

                                    scope.ok = function () {
                                        $uibModalInstance.close();
                                    };

                                    scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                        navigatorManager.goToSharePoint();
                                    };

                                }],

                                resolve: {

                                }
                            });


                        };

                        $scope.init();
                }]
            };
        }]);
})(designerModule);
