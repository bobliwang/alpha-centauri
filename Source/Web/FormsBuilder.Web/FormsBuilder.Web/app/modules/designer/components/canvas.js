(function (designerModule) {

    designerModule.directive('rfCanvas', ['sortableOptionProvider', 'designerApi', '$uibModal', '$log', 'ngToast', 'resourceStrings',
        function (sortableOptionProvider, designerApi, $uibModal, $log, ngToast, resourceStrings) {

            ngToast.settings.verticalPosition = 'bottom';
            ngToast.settings.timeout = 2000;

            return {
                restrict: 'E',
                templateUrl: '/app/modules/designer/components/canvas.tpl.html',

                scope: {
                },

                controller: ['$scope', '$location', '$log', '$timeout', 'appState', 'appStateManager', 'navigatorManager', 'dialogManager',
                    function ($scope, $location, $log, $timeout, appState, appStateManager, navigatorManager, dialogManager) {

                    $scope.appState = appState;
                    $scope.appStateManager = appStateManager;

                    $scope.getForm = function () {
                        designerApi.getForm(appState.listId).then(function (response) {
                            appState.form = response.data;

                            ngToast.success({
                                content: resourceStrings.Msg_FormLoaded
                            });
                        });
                    };

                    $scope.deleteRow = function (row) {
                        if (confirm("Are you sure to delete the selected row?")) {
                            appStateManager.deleteRow(row);
                        }
                    };

                    $scope.keyUp = function (cc, $event) {
                        // delete
                        if ($event.keyCode === 46) {
                            appStateManager.deleteControl(cc);
                        }
                    };

                    $scope.canvasRow_SortableOptions = sortableOptionProvider.getRowSortableOptions($scope);

                    $scope.canvasControl_SortableOptions = sortableOptionProvider.getControlSortableOptions($scope);

                    // TODO: delete later.
                    $scope.items = [];

                    $scope.addRow = function () {
                        appState.form.rows.push({
                            columns: []
                        });
                    };

                    appStateManager.resetForm();
                    $scope.getForm();
                }]
            };
        }])
    ;
})(designerModule);
