(function(designerModule) {
    designerModule.factory("dataTypeControlMappings", function () {
        var self = this;

        self.dataTypeControlMappings = self.dataTypeControlMappings || {};


        return self.dataTypeControlMappings;
    });
})(designerModule);
