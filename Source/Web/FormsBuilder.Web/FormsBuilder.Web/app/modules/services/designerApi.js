var rfApiModule = rfApiModule || angular.module("rf-api", []);

rfApiModule.factory('designerApi', ['$http', '$log', function ($http, $log) {
    var self = this;

    self.designerApi = self.designerApi || {

        getDefaultGroups : function() {
            return [
                {
                    displayName: "General",
                    fields: [
                        {
                            dataType: 11,
                            dataTypeAsString: "Label",
                            displayName: "Label",
                            name: "Label",
                            layoutColumns: 2
                        },
                        {
                            dataType: 12,
                            dataTypeAsString: "Image",
                            displayName: "Image",
                            name: "Image",
                            layoutColumns: 2
                        }
                    ]
                },
                {
                    displayName: "Containers",
                    isContainerGroup: true,
                    fields: [
                        {
                            dataType: 1000,
                            dataTypeAsString: "CanvasRow",
                            displayName: "Row",
                            name: "Row"
                        },
                        {
                            dataType: 1001,
                            dataTypeAsString: "CanvasColumnContainer",
                            displayName: "Column Container",
                            name: "Column Container",
                            layoutColumns: 6
                        }
                    ]
                }
            ];
        },

        getBuiltInToolboxGroups: function() {
            return $http.get("/api/Designer/BuiltInToolboxGroups");
        },

        getListContentTypes: function(listId) {
            return $http.get("/api/Designer/Lists/" + listId + "/ContentTypes");
        },

        getListInfo: function(listId) {
            return $http.get("/api/Designer/Lists/" + listId);
        },

        createDefaultForm: function (request) {
            return $http.post("/api/Designer/CreateDefaultForm", JSON.stringify(request));
        },

        saveForm: function(form) {
            return $http.post("/api/Designer/SaveForm", JSON.stringify(form));
        },

        publishForm: function (form) {
            return $http.post("/api/Designer/PublishForm", JSON.stringify(form));
        },

        getForm: function(listId) {
            return $http.get("/api/Designer/Forms/" + listId);
        },

        submitItem: function(form) {
            return $http.post("/api/Designer/SubmitItem", JSON.stringify(form));
        },

        requestEntityMetadata: function (request) {
            return $http.post("/api/Designer/EntityMetadata", JSON.stringify(request));
        },

        getAvailableUsers: function() {
            return $http.get("/api/Designer/AvailableUsers");
        },

        getDataMappings: function () {
            return $http.get("/api/Designer/DataMappings");
        },

        getListLookupItems: function(lookupControl) {
            return $http.get("/api/Designer/ListLookupItems/" + lookupControl.sourceListId);
        }
    };

    return self.designerApi;

}])

;