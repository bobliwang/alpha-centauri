var rfApiModule = rfApiModule || angular.module("rf-api", []);


rfApiModule.factory('runtimeFunctions', ['$http', '$log', 'runtimeState', function ($http, $log, runtimeState) {
    var self = this;

    self.runtimeFunctions = self.runtimeFunctions || {
        ctrlVal: function (ctrlName) {
            var form = runtimeState.form;

            var ctrlFound = null;

            for (var i = 0; i < form.rows.length && ctrlFound == null; i++) {
                var row = form.rows[i];
                for (var j = 0; j < row.columns.length; j++) {
                    var ctrl = row.columns[j];

                    if (ctrl.name === ctrlName) {
                        ctrlFound = ctrl;
                        break;
                    }
                }
            }

            if (ctrlFound == null) {
                return null;
            }

            return ctrlFound.value;
        },

        isNullOrEmpty : function(val) {
            return val == null || val == "";
        },

        isCtrlEmpty : function(ctrlName) {
            var ctrlVal = self.runtimeFunctions.ctrlVal(ctrlName);

            return self.runtimeFunctions.isNullOrEmpty(ctrlVal);
        }
    };

    return self.runtimeFunctions;
}]);