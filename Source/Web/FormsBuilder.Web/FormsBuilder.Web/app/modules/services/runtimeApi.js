var rfApiModule = rfApiModule || angular.module("rf-api", []);

rfApiModule.factory('runtimeApi', ['$http', '$log', function ($http, $log) {
    var self = this;

    self.runtimeApi = self.runtimeApi || {

        submitItem: function(form) {
            return $http.post("/api/runtime/SubmitItem", JSON.stringify(form));
        }

    };

    return self.runtimeApi;

}])
;

