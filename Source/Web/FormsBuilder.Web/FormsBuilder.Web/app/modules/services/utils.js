var rfUtilsModule = rfUtilsModule || angular.module("rf-utils", []);

rfUtilsModule.factory('rfUtils', ['$http', '$log', function ($http, $log) {
    var self = this;

    if (!self.rfUtils) {
        self.rfUtils = {};


        self.rfUtils.generateGUID = function () {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });

            return uuid;
        };
    }

    return self.rfUtils;

}]);