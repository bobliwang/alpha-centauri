var rfApiModule = rfApiModule || angular.module("rf-api", []);

rfApiModule.factory('resourceStrings', ['$http', '$log', function ($http, $log) {
    var self = this;

    self.resourceStrings = self.resourceStrings || {
        Msg_FormLoaded: "The form is loaded.",
        Msg_FormSaved: "The form is saved successfully.",


        EOF: "EOF"
    };

    return self.resourceStrings;

}])

;