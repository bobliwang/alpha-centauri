var rfDialogsModule = rfDialogsModule || angular.module("rf-dialogs", ['ui.bootstrap']);

rfDialogsModule.factory('dialogManager', ['$uibModal', '$http', '$log', function ($uibModal, $http, $log) {
    var self = this;

    if (!self.dialogManager) {

        self.dialogManager = {};
        self.dialogManager.confirm = function (message, title, size) {

            size = size || 'lg';

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/app/modules/services/dialog.confirm.tpl.html',
                size: size,
                controller: ["$scope", "$uibModalInstance", function (scope, $uibModalInstance) {

                    scope.title = title;
                    scope.message = message;

                    scope.setResult = function(result) {
                        $uibModalInstance.close({ result: result });
                    };
                }],
                resolve: {
                }
            });

            return modalInstance.result;
        };
    }

    return self.dialogManager;

}]);