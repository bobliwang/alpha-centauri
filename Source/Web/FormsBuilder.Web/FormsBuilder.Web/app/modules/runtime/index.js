var runtimeModule = angular.module("rf-runtime", ['ui.bootstrap', 'ui.sortable', 'ngToast', 'rf-api', 'rf-utils', 'localytics.directives', 'uiSwitch']);

(function (runtimeModule) {
    runtimeModule.utils = runtimeModule.utils || {};
    runtimeModule.utils.controlTemplateUrl = function (controlName) {
        return '/app/modules/runtime/controls/' + controlName + '.tpl.html';
    };
    
    runtimeModule
        .factory("runtimeState", function () {
            var self = this;

            self.runtimeState = self.runtimeState || {
                listId: (ac.runtime.globalSettings.ListId || ac.runtime.globalSettings.List)
            };

            return self.runtimeState;
        })
        .controller('FormRuntimeController', ['$scope', '$log', '$timeout', 'designerApi', 'runtimeApi', 'runtimeState', 'ngToast',
            function ($scope, $log, $timeout, designerApi, runtimeApi, runtimeState, ngToast) {

                ngToast.settings.verticalPosition = 'bottom';
                ngToast.settings.timeout = 2000;

                $scope.runtimeState = runtimeState;

                $scope.init = function () {
                    designerApi.getForm(runtimeState.listId).then(function (response) {
                        runtimeState.form = response.data;
                    });
                };

                $scope.submitItem = function () {
                    $scope.message = "";
                    runtimeApi.submitItem(runtimeState.form).then(function (response) {
                        ngToast.success({
                            content: "The list item data is saved."
                        });

                        $scope.message = "saved..";
                    });
                };

                $scope.init();
            }
        ]);


})(runtimeModule);

