(function (runtimeModule) {

    var controlName = "Lookup";

    runtimeModule.directive('rfRuntime' + controlName, ['$log', '$timeout', 'designerApi', function ($log, $timeout, designerApi) {

        return {
            restrict: 'E',
            templateUrl: runtimeModule.utils.controlTemplateUrl(controlName),

            scope: {
                control: "="
            },

            controller: function ($scope, $log, $timeout) {

                designerApi.getListLookupItems($scope.control).then(function (response) {
                    $log.info(response.data);

                    $scope.listLookupItems = response.data;
                });
            }
        };
    }]);
})(runtimeModule);

