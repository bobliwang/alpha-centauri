(function (runtimeModule) {

    var controlName = "Control";

    runtimeModule.directive('rfRuntime' + controlName, ['recursionHelper', 'runtimeState', function (recursionHelper, runtimeState) {

        return {
            restrict: 'E',
            templateUrl: runtimeModule.utils.controlTemplateUrl(controlName),

            scope: {
                control: "="
            },

            compile: function (element) {
                // Use the compile function from the RecursionHelper,
                // And return the linking function(s) which it returns
                return recursionHelper.compile(element);
            },


            controller: ['$scope', '$log', '$timeout', 'runtimeState', 'runtimeFunctions', function ($scope, $log, $timeout, runtimeState, runtimeFunctions) {

                $scope.runtimeState = runtimeState;
                
                $scope.$watch("control", function (control) {
                    if (control) {
                        control.enabled = true;
                        control.visible = true;

                        if (control.ruleArray && control.ruleArray.rules) {

                            var ruleGroups = _.groupBy(control.ruleArray.rules, function(x) {
                                return x.ruleType;
                            });

                            var hideGroup = ruleGroups["Hide"];

                            if (hideGroup) {
                                $scope.$watch(function () {

                                    var value = false;

                                    _.each(hideGroup, function (rule) {

                                        value = value || $scope.$eval(rule.expression, runtimeFunctions);

                                    });

                                    return value;
                                }, function (val) {
                                    control.visible = !val;
                                });
                            }
                        }
                    }
                });
            }]
        };
    }]);

})(runtimeModule);

