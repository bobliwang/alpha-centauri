(function (runtimeModule) {

    var controlName = "Peoplepicker";

    runtimeModule.directive('rfRuntime' + controlName, ['$log', '$timeout', 'designerApi',

        function ($log, $timeout, designerApi) {

            return {
                restrict: 'E',
                templateUrl: runtimeModule.utils.controlTemplateUrl(controlName),

                scope: {
                    control: "="
                },

                controller: function ($scope, $log, $timeout) {

                    designerApi.getAvailableUsers().then(function (response) {
                        $log.info(response.data);

                        $scope.users = response.data;
                    });

                }
            };
        }
    ]);

})(runtimeModule);