(function (runtimeModule) {

    var controlName = "Fileupload";

    runtimeModule.directive('rfRuntime' + controlName, ['$log', '$timeout', function ($log, $timeout) {

        return {
            restrict: 'E',
            templateUrl: runtimeModule.utils.controlTemplateUrl(controlName),

            scope: {
                control: "="
            },

            controller: function ($scope, $log, $timeout) {

            }
        };
    }]);

})(runtimeModule);
