(function (runtimeModule) {

    var controlName = "Image";

    runtimeModule.directive('rfRuntime' + controlName, ['$log', '$timeout',
        function ($log, $timeout) {
            return {
                restrict: 'E',
                templateUrl: runtimeModule.utils.controlTemplateUrl(controlName),

                scope: {
                    control: "="
                },

                controller: ["$scope", "$log", "$timeout", function ($scope, $log, $timeout) {
                    
                }]
            };
        }])
    ;
})(runtimeModule);
