(function(runtimeModule) {

    var controlName = "Repeater";

    runtimeModule.directive('rfRuntime' + controlName, ['$log', '$timeout', function ($log, $timeout) {
        return {
            restrict: 'E',
            templateUrl: runtimeModule.utils.controlTemplateUrl(controlName),

            scope: {
                control: "="
            },

            controller: function ($scope, $log, $timeout) {

                function rowsDataChanged() {

                    var i = 0;
                    _.each($scope.control.rows, function(row) {
                        _.each(row.columns, function(col) {
                            col.repeaterContext = {
                                rowIndex: i
                            };
                        });

                        i++;
                    });
                }

                $scope.newRow = function() {
                    $scope.control.rows = $scope.control.rows || [];
                    if ($scope.control.templateRow) {

                        var row = { columns: [] };

                        for (var i = 0; i < $scope.control.templateRow.columns.length; i++) {
                            var clonedCtrl = _.clone($scope.control.templateRow.columns[i]);
                            clonedCtrl.$$hashKey = _.uniqueId('control_');

                            row.columns.push(clonedCtrl);
                        }

                        $scope.control.rows.push(row);

                        rowsDataChanged();
                    }
                };

                $scope.removeRow = function(row) {
                    $scope.control.rows = _.reject($scope.control.rows, function (x) {
                        return x === row;
                    });

                    rowsDataChanged();
                };

                $scope.init = function() {
                    for (var i = 0; i < $scope.control.initialRows; i++) {
                        $scope.newRow();
                    }
                };

                $scope.init();
            }
        };
    }])
    ;

})(runtimeModule);