(function (runtimeModule) {

    var controlName = "Label";

    runtimeModule.directive('rfRuntime' + controlName, ['$log', '$timeout', function ($log, $timeout) {

        return {
            restrict: 'E',
            templateUrl: runtimeModule.utils.controlTemplateUrl(controlName),

            scope: {
                control: "="
            },

            controller: function ($scope, $log, $timeout) {

                var control = $scope.control;

                $scope.getLabelText = function() {
                    var text = (control.text || control.defaultValue || control.displayName || control.name);

                    if (control.repeaterContext) {
                        return text.replace('${rowIndex}', control.repeaterContext.rowIndex);
                    }
                    
                    return text;
                };
            }
        };
    }]);

})(runtimeModule);
