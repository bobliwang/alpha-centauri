using System;
using System.Threading.Tasks;
using FormsBuilder.Common.Domain.Form;
using SharePointO365.RestClient.Models;

namespace FormsBuilder.Web.Services
{
    public interface IFormService
    {
        Task<Form> CreateDefaultForm(ListInfo listInfo);

        Task<Form> GetForm(Guid listId);

        Task<Guid> PublishForm(Form form);

        Task<Guid> SaveForm(Form form);
    }
}