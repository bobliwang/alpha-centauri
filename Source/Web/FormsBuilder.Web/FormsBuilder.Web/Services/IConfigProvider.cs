﻿namespace FormsBuilder.Web.Services
{
    public interface IConfigProvider
    {
        string Get(string name);
    }
}