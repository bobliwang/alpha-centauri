﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using FormsBuilder.Common.Configuration;
using FormsBuilder.Common.Controls;
using FormsBuilder.Common.Domain.Entities;
using FormsBuilder.Common.Domain.Form;
using FormsBuilder.Common.Domain.Metadata;
using FormsBuilder.Common.Domain.Repositories;
using FormsBuilder.Web.Configuration;
using Newtonsoft.Json;
using SharePointO365.RestClient;
using SharePointO365.RestClient.Models;

namespace FormsBuilder.Web.Services
{
    public class FormService : IFormService
    {
        public FormService(SpoRestClient spoRestClient, IFormRepository formRepository, Settings settings, TypeMappingManager typeMappingManager, IListService listService)
        {
            this.SpoRestClient = spoRestClient;
            this.FormRepository = formRepository;
            this.Settings = settings;
            this.TypeMappingManager = typeMappingManager;
            this.ListService = listService;
        }

        public SpoRestClient SpoRestClient { get; private set; }
        
        public IFormRepository FormRepository { get; private set; }

        public Settings Settings { get; private set; }

        public IListService ListService { get; private set; }
        
        public TypeMappingManager TypeMappingManager { get; private set; }

        public async Task<Form> CreateDefaultForm(ListInfo listInfo)
        {
            var entityMetadata = await this.ListService.GetEntityMetadataFromSharePoint(listInfo);

            var form = new Form { ListId = entityMetadata.ListId };

            foreach (var field in entityMetadata.Fields)
            {
                var row = new RowControl();

                row.Columns.Add(new Label { LayoutColumns = 2, ControlType = ControlType.Label, DisplayName = field.DisplayName, Name = field.Name });

                var control = field.CreateControl();
                row.Columns.Add(control);

                form.Rows.Add(row);
            }

            return form;
        }

        public async Task<Form> GetForm(Guid listId)
        {
            var formInfo = this.FormRepository.FormInfos.FirstOrDefault(x => x.ListId == listId);

            Form form = null;
            if (formInfo != null)
            {
                try
                {
                    form = JsonConvert.DeserializeObject<Form>(formInfo.Contents);

                }
                catch
                {
                }
            }

            if (form == null)
            {
                var listInfo = await this.SpoRestClient.GetListInfo(listId);

                form = await this.CreateDefaultForm(listInfo);
            }

            if (formInfo != null)
            {
                form.Id = formInfo.Id;
                form.ListId = formInfo.ListId ?? Guid.Empty;
            }
            
            return form;
        }

        public async Task<Guid> PublishForm(Form form)
        {
            var guid = await this.SaveForm(form);

            var contentTypes = await this.SpoRestClient.GetListContentTypes(form.ListId);
            var appInfo = await this.SpoRestClient.GetAppInstanceByProductIdAsync(new Guid(this.Settings.ProductId));

            var contentType = contentTypes.Value.FirstOrDefault(x => x.StringId.Equals(form.ContentTypeId));
            var urlInfo = this.GetContentTypeUrlsForPublish(appInfo, contentType.FormPath, contentType.StringId);

            await this.SpoRestClient.UpdateListContentTypeUrls(form.ListId, form.ContentTypeId, urlInfo);

            return guid;
        }

        public async Task<Guid> SaveForm(Form form)
        {
            var tenantId = await this.SpoRestClient.GetTenantId();

            FormInfo formInfo;
            if (form.Id == null || form.Id.Value == Guid.Empty)
            {
                form.Id = Guid.NewGuid();
                formInfo = this.CreateNewFormInfo(form, tenantId);
            }
            else
            {
                formInfo = this.FormRepository.LookupFormInfoById(form.Id.Value);
                if (formInfo == null)
                {
                    formInfo = this.CreateNewFormInfo(form, tenantId);
                }
                else
                {
                    formInfo.CustomerId = tenantId;
                    formInfo.Contents = JsonConvert.SerializeObject(form);
                    formInfo.ContentTypeId = form.ContentTypeId;
                    this.FormRepository.UpdateFormInfo(formInfo);
                }
            }

            return formInfo.Id;
        }

        private ContentTypeUrlsInfo GetContentTypeUrlsForPublish(AppInstanceInfo formsAppDetails, string targetRelativeUrl, string contentTypeId)
        {
            var publishContentTypeUrls = new ContentTypeUrlsInfo();

            publishContentTypeUrls.EditFormUrl = this.GetFormUrl(formsAppDetails, targetRelativeUrl, contentTypeId, "Edit");
            publishContentTypeUrls.DisplayFormUrl = this.GetFormUrl(formsAppDetails, targetRelativeUrl, contentTypeId, "Display");
            publishContentTypeUrls.NewFormUrl = this.GetFormUrl(formsAppDetails, targetRelativeUrl, contentTypeId, "New");

            return publishContentTypeUrls;
        }

        private string GetFormUrl(AppInstanceInfo formsAppDetails, string serverRelativeUrl, string contentTypeId, string mode, string launchPage = "launchRuntime")
        {
            StringBuilder urlBuilder = new StringBuilder();

            urlBuilder
                .Append(string.Format("{0}/pages/{1}.aspx", HttpUtility.UrlPathEncode(formsAppDetails.AppName), launchPage))
                .Append("?")
                .Append(string.Format("SPAppWebUrl={0}", HttpUtility.UrlPathEncode(formsAppDetails.AppWebUrl)))
                .Append("&")
                .Append(string.Format("SPHostUrl={0}", HttpUtility.UrlPathEncode(formsAppDetails.HostUrl)))
                .Append("&")
                .Append(string.Format("remoteAppUrl={0}", HttpUtility.UrlPathEncode(formsAppDetails.RemoteAppUrl)))
                .Append("&")
                .Append(string.Format("ctype={0}", contentTypeId))
                .Append("&")
                .Append(string.Format("wtg={0}", serverRelativeUrl))
                .Append("&")
                .Append(string.Format("mode={0}", mode));

            return urlBuilder.ToString();
        }

        private FormInfo CreateNewFormInfo(Form form, Guid tenantId)
        {
            var formInfo = new FormInfo
            {
                Id = form.Id.Value,
                CustomerId = tenantId,
                SourceUri = string.Empty,
                ListId = form.ListId,
                Contents = JsonConvert.SerializeObject(form),
                ContentTypeId = form.ContentTypeId
            };

            this.FormRepository.SaveFormInfo(formInfo);

            return formInfo;
        }
    }
}