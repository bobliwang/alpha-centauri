﻿using System;
using FormsBuilder.Common.Services;

namespace FormsBuilder.Web.Services
{
    using Newtonsoft.Json;
    using StackExchange.Redis;


    public class RedisCacheService : ICacheService
    {
        public RedisCacheService(ConnectionMultiplexer connMultiplexer)
        {
            this.ConnMultiplexer = connMultiplexer;
        }

        public ConnectionMultiplexer ConnMultiplexer { get; private set; }

        public virtual object Get(string key)
        {
            var redisVal = this.ConnMultiplexer.GetDatabase().StringGet(key);

            if (!redisVal.HasValue)
            {
                return null;
            }

            return redisVal.ToString();
        }

        public virtual void Put(string key, object value, DateTime expireOn, TimeSpan slidingExpireTimespan)
        {
            var timespan = TimeSpan.FromSeconds(Math.Max(expireOn.Subtract(DateTime.UtcNow).TotalSeconds, slidingExpireTimespan.TotalSeconds));

            this.ConnMultiplexer.GetDatabase().StringSet(key, JsonConvert.SerializeObject(value), timespan);
        }

        public void ClearKey(string key)
        {
            this.ConnMultiplexer.GetDatabase().KeyDelete(key);
        }
    }
}