using System.Collections.Generic;
using System.Threading.Tasks;
using SharePointO365.RestClient.Models;

namespace FormsBuilder.Web.Services
{
    public interface IWebService
    {
        Task<IList<UserInfo>> GetAvailableUsers();
    }
}