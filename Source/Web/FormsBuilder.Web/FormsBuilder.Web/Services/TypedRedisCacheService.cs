using System;
using FormsBuilder.Common.Domain;
using FormsBuilder.Common.Services;
using Newtonsoft.Json;

namespace FormsBuilder.Web.Services
{
    public class TypedRedisCacheService<T>: ITypedRedisCacheService<T> where T : class, ICacheable
    {
        private readonly ICacheService cacheService;

        public TypedRedisCacheService(ICacheService redisCacheService)
        {
            this.cacheService = redisCacheService;
        }

        public T Get(string key)
        {
            var strVal = this.cacheService.Get(key) as string;

            if (strVal == null)
            {
                return null;
            }

            return JsonConvert.DeserializeObject<T>(strVal);
        }

        public void Put(T value)
        {
            this.cacheService.Put(value.CacheKey, value, DateTime.MaxValue, TimeSpan.Zero);
        }

        public void Clear(string key)
        {
            this.cacheService.ClearKey(key);
        }
    }
}