using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FormsBuilder.Common.Domain.Metadata;
using SharePointO365.RestClient.Models;

namespace FormsBuilder.Web.Services
{
    public interface IListService
    {
        Task<ListInfo> GetListInfo(Guid listId);

        Task<IList<ContentTypeInfo>> GetContentTypes(Guid listId);

        Task<EntityMetadata> GetEntityMetadataFromSharePoint(ListInfo listInfo);

        Task<IList<ListItem>> GetListData(string sourceListId);
    }
}