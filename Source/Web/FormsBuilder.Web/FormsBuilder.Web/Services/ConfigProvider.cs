using System.Configuration;

namespace FormsBuilder.Web.Services
{
    public class ConfigProvider : IConfigProvider
    {
        public string Get(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }
    }
}