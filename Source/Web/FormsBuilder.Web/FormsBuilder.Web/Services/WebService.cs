﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SharePointO365.RestClient;
using SharePointO365.RestClient.Models;

namespace FormsBuilder.Web.Services
{
    public class WebService : IWebService
    {
        public WebService(SpoRestClient spoRestClient)
        {
            this.SpoRestClient = spoRestClient;
        }

        public SpoRestClient SpoRestClient { get; private set; }

        public async Task<IList<UserInfo>> GetAvailableUsers()
        {
            var userInfos = await this.SpoRestClient.GetInfo<UserInfos>("web/siteusers");

            return userInfos.Value;
        }
    }
}