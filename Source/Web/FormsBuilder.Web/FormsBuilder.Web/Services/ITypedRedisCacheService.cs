using FormsBuilder.Common.Domain;

namespace FormsBuilder.Web.Services
{
    public interface ITypedRedisCacheService<T> where T : ICacheable
    {
        T Get(string id);

        void Put(T value);

        void Clear(string id);
    }
}