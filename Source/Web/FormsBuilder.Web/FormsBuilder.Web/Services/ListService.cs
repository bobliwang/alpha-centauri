﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using FormsBuilder.Common.Domain.Metadata;
using FormsBuilder.Web.Configuration;
using SharePointO365.RestClient;
using SharePointO365.RestClient.Models;

namespace FormsBuilder.Web.Services
{
    public class ListService : IListService
    {
        private readonly string[] UnsupportedFieldNames = new[]
        {
            "ItemChildCount",
            "FolderChildCount"
        };

        private readonly SpoRestClient spoRestClient;

        private readonly TypeMappingManager typeMappingManager;

        public ListService(SpoRestClient spoRestClient, TypeMappingManager typeMappingManager)
        {
            this.spoRestClient = spoRestClient;
            this.typeMappingManager = typeMappingManager;
        }

        public async Task<ListInfo> GetListInfo(Guid listId)
        {
            var listInfoTask = this.spoRestClient.GetListInfo(listId);
            var listContentTypesTask = this.spoRestClient.GetListContentTypes(listId);

            var listInfo = await listInfoTask;
            listInfo.ContentTypes = (await listContentTypesTask).Value;

            foreach (var ct in listInfo.ContentTypes)
            {
                ct.Fields = (await this.spoRestClient.GetListContentTypeFields(listId, ct.Id.StringValue)).Value;
            }

            return listInfo;
        }

        public async Task<IList<ContentTypeInfo>> GetContentTypes(Guid listId)
        {
            var contentTypes = await this.spoRestClient.GetListContentTypes(listId);

            return contentTypes.Value;
        }

        public async Task<EntityMetadata> GetEntityMetadataFromSharePoint(ListInfo listInfo)
        {
            var entityMetadata = new EntityMetadata
            {
                Name = listInfo.Title,
                DisplayName = listInfo.Title,
                ListId = listInfo.Id
            };

            var fields = new List<FieldInfo>();

            foreach (var contentType in listInfo.ContentTypes)
            {
                fields.AddRange(contentType.Fields);
            }

            fields = fields.Where(x => !x.Hidden && this.typeMappingManager.SupportedFieldTypes.Contains(x.FieldType)).Where(x => !UnsupportedFieldNames.Contains(x.InternalName)).ToList();
            

            foreach (var field in fields)
            {
                var fieldMetadata = await this.CreateFieldMetadata(field);
                entityMetadata.Fields.Add(fieldMetadata);
            }

            return entityMetadata;
        }

        public async Task<IList<ListItem>> GetListData(string sourceListId)
        {
            var items = await this.spoRestClient.GetListData(new Guid(sourceListId));

            return items.Value;
        }

        private async Task<FieldMetadata> CreateFieldMetadata(FieldInfo fieldInfo)
        {
            FieldMetadata value;

            if (fieldInfo.FieldType == SharePointO365.RestClient.Enums.FieldType.Lookup)
            {
                var xdoc = XDocument.Load(new StringReader(fieldInfo.SchemaXml));

                var listId = xdoc.Root.Attribute("List").Value;
                listId = listId.Substring(1, listId.Length - 2);

                // slow ...
                var listInfo = await this.spoRestClient.GetListInfo(new Guid(listId));
                
                value = new LookupFieldMetadata
                {
                    SourceListId = listId,
                    SourceListName = listInfo.Title
                };
            }
            else
            {
                value = new FieldMetadata();
            }
#if DEBUG
            value.RawData = fieldInfo;
#endif

            value.Name = fieldInfo.InternalName;
            value.DisplayName = fieldInfo.Title;
            value.DataType = this.typeMappingManager.MapToDataType(fieldInfo.FieldType);
            value.Required = fieldInfo.Required;
            value.LayoutColumns = 3;

            return value;
        }
    }
}